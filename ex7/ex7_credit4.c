#include<stdio.h>
#include<stdlib.h>
#include<limits.h>
int main(int argc,char *argv[]){
	int bugs = 100;
	double bug_rate = 1.2;
	printf("You have %d bugs at the imaginary rate of %f.\n",
			bugs, bug_rate);
	long universe_of_defects = LONG_MAX; // long=64bits
	printf("[long]The entire universe has %ld bugs. \n",
			universe_of_defects);
	unsigned long universe_of_defects_un=ULONG_MAX;
	printf("[unsigned long]The entire universe has %lu bugs. \n",
			universe_of_defects_un);
	//printf("size of long =%lu\n",sizeof(long));  //prove that the size of long is 64 bits
	double expected_bugs = bugs * bug_rate;
	printf("You are expected to have %f bugs.\n",expected_bugs);
	double part_of_universe = expected_bugs / universe_of_defects;
	printf("That is only a %e portion the universe.\n",part_of_universe);
	char nul_byte='\0';
	int care_percentage = bugs * nul_byte;
	printf("Which means you should care %d%%.\n",care_percentage);
	return 0;
}
