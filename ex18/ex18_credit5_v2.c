#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
//#define DEBUG
void die(const char *message)
{
	if(errno) {
		perror(message);
	} else {
		printf("ERROR: %s\n",message);
	}
	exit(1);
}
typedef int (*compare_cb)(int a,int b);
typedef int *(*sort_method)(int *numbers,int count, compare_cb cmp);
int *bubble_sort(int *numbers, int count, compare_cb cmp)
{
	int temp = 0;
	int i = 0;
	int j = 0;
	int *target = malloc(count *sizeof(int));
	memcpy(target, numbers, count * sizeof(int));
	for(i=0;i<count;i++){
		for(j=0;j<count-1;j++){
			if(cmp(target[j], target[j+1]) > 0){
				temp = target[j+1];
				target[j+1] = target[j];
				target[j]=temp;
			}
		}
	}
	return target;
}
int *quick_sort(int *target,int left,int right)
{
	/*int temp = 0;
	int i=0;
	int j=0;*/
	//int *target = malloc(count*sizeof(int));
	//memcpy(target, numbers, count * sizeof(int));
	if(left< right){
		int i=left+1, j=right, splitting=target[left],tmp;
		do{
			while(i<right && target[i]<=splitting)i++;
			while(j>left && target[j]>splitting)j--;
			if(i<j){
				tmp=target[i];target[i]=target[j];target[j]=tmp;
			}
		}while(i<j);
			tmp=target[j]; target[j]=target[left]; target[left]=tmp; //swap data[j] and splitting
			return quick_sort(target,left,j-1);
			return quick_sort(target,j+1,right);
	}
	return target;
}
void merge(int left, int right,int *numbers, int count, compare_cb cmp)
{
	//printf("right=%d, left=%d\n",right,left);
	int mid=(left+right)/2;
	int *tmp=malloc(count * sizeof(int));
	int i,j,k;
	for(k=0,i=left,j=mid+1;i<=mid || j<=right;++k){
		if(i>mid)
			tmp[k]=numbers[j++];
		else if(j>right)
			tmp[k]=numbers[i++];
		else if(cmp(numbers[i],numbers[j])<0)
			tmp[k]=numbers[i++];
		else
			tmp[k]=numbers[j++];
	}

/*	for(i=left;i<=right;i++){
		printf("%d ",numbers[i]);
	}
	printf("\n");
*/
	for(i=left, k=0;i<=right;++k,++i){
		numbers[i]=tmp[k];
	}
#ifdef DEBUG
	for(i=left;i<=right;i++){
		printf("%d ",numbers[i]);
	}
	printf("\n");
#endif
	free(tmp);
}
int *merge_sort(int *numbers,int left, int right,int count, compare_cb cmp)
{
	/*int temp = 0;
	int i=0;
	int j=0;*/
	int mid = (left+right)/2;
	if(left<right){
		merge_sort(numbers, left, mid, count, cmp);
		merge_sort(numbers, mid+1, right, count, cmp);
		merge(left, right, numbers, count, cmp);
	}

	return numbers;
}
int sorted_order(int a,int b){
	return a - b;
}
int reverse_order(int a, int b){
	return b - a;
}
int strange_order(int a, int b){
	if( a==0||b==0){
		return 0;
	}else {
		return a % b;
	}
}
int* Mergesorting(int *numbers, int count, compare_cb cmp)
{
	int *unsorted = malloc(count *sizeof(int));
	memcpy(unsorted, numbers, count * sizeof(int));
	/*
	int *sorted = merge_sort(unsorted, 0, count-1, count, cmp);
	if(!sorted) die("Failed to sort as requested.");
	printf("After sorting using merge sort:");
	for(i=0;i<count;i++){
		printf("%d ",sorted[i]);
	}
	printf("\n");
	free(sorted);*/
	return merge_sort(unsorted, 0, count-1, count, cmp);
}

int* Quicksorting(int *numbers, int count, compare_cb cmp)
{
	int *unsorted = malloc(count *sizeof(int));
	memcpy(unsorted, numbers, count * sizeof(int));
	/*
	int *sorted = quick_sort(unsorted, 0,count-1);
	if(!sorted) die("Failed to sort as requested.");
	printf("After sorting using quick sort:");
	for(i=0;i<count;i++){
		printf("%d ",sorted[i]);
	}
	printf("\n");
	return sorted*/
		return quick_sort(unsorted, 0, count-1);
}
void test_sorting(int *numbers, int count, compare_cb cmp,sort_method sort)
{
	int i=0;
	int *sorted = sort(numbers, count, cmp);
	if(!sorted) die("Failed to sort as requested.");
	for(i=0;i<count;i++){
		printf("%d ",sorted[i]);
	}
	printf("\n");
	free(sorted);
}
int main(int argc,char *argv[]){
	if(argc < 2)die("USAGE: ex18 4 3 1 5 6");
	int count = argc-1;
	int i=0;
	char **inputs = argv+1;
	int *numbers = malloc(count * sizeof(int));
	if(!numbers) die("Memory error.");
	for(i=0;i<count;i++){
		numbers[i] = atoi(inputs[i]);
	}
	test_sorting(numbers, count, sorted_order,bubble_sort);
	test_sorting(numbers, count, reverse_order,bubble_sort);
	test_sorting(numbers, count, strange_order,bubble_sort);
	test_sorting(numbers, count, sorted_order,Mergesorting);
	test_sorting(numbers, count, sorted_order,Quicksorting);
	free(numbers);
	return 0;
}


