#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#ifdef DEBUG
#define DEBUG
#endif
#define SIZE 100
#define getNumberInArrar(arr) (sizeof(arr),sizeof(arr[0]))
char* strcpy_v1(char *dest, const char *src)
{
	const char *p;
	char *q;
	for (p=src,q=dest; *p!='\0'; p++, q++)
	*q=*p;
	*q = '\0';
	return dest;
}
char* strcpy_v2(char *dest, const char *src)
{
	char *ptr;
	ptr = dest;
	while(*dest++=*src++);
	return ptr;
}
char* strcpy_v3(char *dest, const char *src)
{
	int i=0;
	while(src[i] != '\0'){
		dest[i++]=src[i];
	}

	dest[i]='\0';
	return dest;
}

int main(int argc,char *argv[])
{
	int i=argc-1;
	printf("Now, i's value:%d\n",i);
	while( i >= 0) {
		printf("arg %d: %s\n",i,argv[i]);
		i--;
	}
	char **states;
	states=(char **)malloc(argc * sizeof(char*));
	for(i=0;i<argc;i++)
		states[i]=(char*)malloc(SIZE * sizeof(char));
	int num_states = argc-1;
	//int i=0;//compile error!
	i=num_states;
	while( i >= 0) {
		strcpy(states[i],argv[i]);
		printf("state %d: %s\n",i , states[i]);
		i--;
	}
	for(i=0;i<argc;i++){
		free(states[i]);
	}
	free(states);
	return 0;
}
