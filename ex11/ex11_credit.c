#include<stdio.h>
#include<stdlib.h>
#ifdef DEBUG
#define DEBUG
#endif
#define getNumberInArrar(arr) (sizeof(arr),sizeof(arr[0]))
int main(int argc,char *argv[])
{
	int i=argc-1;
	printf("Now, i's value:%d\n",i);
	while( i >= 0) {
		printf("arg %d: %s\n",i,argv[i]);
		i--;
	}
	char *states[] = {
		"California", "Oregon",
		"Washington", "Texas"
	};
	int num_states = 4;
	//int i=0;//compile error!
	i=num_states-1;
	while( i >= 0) {
		printf("state %d: %s\n",i , states[i]);
		i--;
	}
	return 0;
}
