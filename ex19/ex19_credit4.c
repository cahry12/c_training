#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include "ex19_credit4.h"
Object MapProto = {
    .init = Map_init,
    .move = Map_move,
    .attack = Map_attack
};
int main(int argc,char *argv[])
{
    // Simple way to setup the randomness
    srand(time(NULL));
    // Make our map to work with
    Map *game = NEW(Map, "The Hall of the Minotaur.");
    while (process_input(game)) {
      
    }
    
    return 0;
}
