#include<stdio.h>
#include<stdlib.h>
int main(int argc,char *argv[])
{
	char full_name[12] = {
		'Z', 'e', 'd',
		' ', 'A', '.', ' ',
		'S', 'h', 'a', 'w'
	};
	char full_name_error[11] = {
		'Z', 'e', 'd',
		' ', 'A', '.', ' ',
		'S', 'h', 'a', 'w'
	};
	printf("The size of full_name (char[]): %ld\n",sizeof(full_name));
	printf("The number of chars: %ld\n",
			sizeof(full_name) / sizeof(char));
	printf("The size of full_name_error (char[]): %ld\n",sizeof(full_name_error));
	printf("The number of full_name_error: %ld\n",
			sizeof(full_name_error) / sizeof(char));
	printf("full_name=\"%s\" and full_name_errors=\"%s\"\n",
			full_name,full_name_error);

	return 0;
}
