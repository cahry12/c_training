#include<stdio.h>
#include<stdlib.h>
int main(int argc,char *argv[])
{
	int areas[]= {'Z', 12, 13, 14, 20};
	printf("The size of an int: %ld\n",sizeof(int));
	printf("The size of areas (int[]): %ld\n",
			sizeof(areas));
	printf("The number of ints in areas: %ld\n",
			sizeof(areas) / sizeof(int));
	printf("The first area is %d, the 2nd %d.\n",areas[0], areas[1]);

	return 0;
}
