#include<stdio.h>
#include<stdlib.h>
int main(int argc,char *argv[])
{
	const int con_areas[]= {10, 12, 13, 14, 20};
	int areas[]= {10, 12, 13, 14, 20};
	char name[] = "Zed";
	char full_name[] = {
		'Z', 'e', 'd',
		' ', 'A', '.', ' ',
		'S', 'h', 'a', 'w', '\0'
	};
	for(int i=0;i<4;++i){
	//	con_areas[i]=100; compile error!
		areas[i]=100;
	}
	name[0]='T';
	full_name[0]='T'; full_name[4]='B'; full_name[7]='C';
	printf("The size of an int: %ld\n",sizeof(int));
	printf("The size of areas (int[]): %ld\n",
			sizeof(areas));
	printf("The number of ints in areas: %ld\n",
			sizeof(areas) / sizeof(int));
	printf("The first area is %d, the 2nd %d.\n",areas[0], areas[1]);
	printf("The size of a char: %ld\n",sizeof(char));
	printf("The size of name (char[]): %ld\n",sizeof(name));
	printf("The number of chars: %ld\n",sizeof(name) /sizeof(char));
	printf("The size of full_name (char[]): %ld\n",sizeof(full_name));
	printf("The number of chars: %ld\n",
			sizeof(full_name) / sizeof(char));
	printf("name=\"%s\" and full_name=\"%s\"\n",name,full_name);

	return 0;
}
