public abstract class Stack<T> {
	protected int count;
	abstract public void push(T value);
	abstract public T pop();
	abstract public boolean isempty();
	abstract public void print();
	public int size()
	{
		return count;
	}
}
