#!/bin/sh
set -e
echo "Automatically creating database"
./ex17 db.dat_credit5 c

echo "Automatically setting data(including cahry, hamilton and frank)"
./ex17 db.dat_credit5 s 1 cahry cahry@ncku.edu.tw
./ex17 db.dat_credit5 s 2 hamilton hamilton@ncku.edu.tw
./ex17 db.dat_credit5 s 3 frank frank@gmail.com

echo "Automatically printing all data records"
./ex17 db.dat_credit5 l

echo "\nAutomatically deleting record by id 1"
./ex17 db.dat_credit5 d 1

echo "Automatically printing all data records"
./ex17 db.dat_credit5 l

echo "\nAutomatically getting record by id 2"
./ex17 db.dat_credit5 g 2
