public class ArrayStack<T> extends Stack<T> {
	private int capacity;
	private T[] array;
	public ArrayStack()
	{
		this(1024);
	}
	@SuppressWarnings("unchecked")
	public ArrayStack(int capacity)
	{
		this.capacity = capacity;
		array = (T[]) new Object[capacity];
	}
	@SuppressWarnings("unchecked")
	@Override
		public void push(T value)
		{
			if(count==array.length){
				T[] newArray = (T[]) new Object[array.length + capacity];
				System.arraycopy(array, 0, newArray, 0, array.length);
				array = newArray;
			}
			array[count++] = value;
		}
	@SuppressWarnings("unchecked")
	@Override
		public T pop() {
			T value = array[--count];
			array[count] = null;
			return value;
		}
	@SuppressWarnings("unchecked")
	@Override
		public boolean isempty() {
			if(count>0)
				return false;
			return true;
		}
	@SuppressWarnings("unchecked")
	@Override
		public void print() {
			for(int i=0;i<count;i++)
				System.out.print(array[i]+" ");
			System.out.println();
		}
	@SuppressWarnings("unchecked")
	public static void main(String[] args)
	{
		System.out.println("Creating stack...");
		ArrayStack stack = new ArrayStack();
		stack.push(10);
		stack.push(30);
		stack.push(50);
		stack.print();
		stack.pop();
		System.out.println("After pop one...");
		stack.print();
	}
}
