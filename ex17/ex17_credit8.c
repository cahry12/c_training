#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<errno.h>
#ifndef INSERT_SIZE
#define INSERT_SIZE 5
#endif
#ifndef LIMIT
#define LIMIT_SIZE 10
#endif
typedef struct Stack {
	int *nodes;
	int size;
	int capacity;
}STACK;
STACK *stack=NULL;
int is_enable_resize = 0;
void free_stack()
{
	if(stack) {
		if(stack->nodes){
			free(stack->nodes);
		}
		free(stack);
	}
}
void die(const char *message)
{
	if(errno){
		perror(message);
	} 
	else {
		printf("ERROR: %s\n",message);
	}
	exit(1);
}
STACK *new()
{
	STACK *stack = malloc(sizeof(STACK));
	stack->nodes = malloc(sizeof(int)*LIMIT_SIZE);
	stack->size = 0;
	stack->capacity = LIMIT_SIZE;
	return stack;
}
void resize(int max)
{
	int *nodes = malloc(sizeof(int)*max);
	for(int i=0;i<stack->size;++i){
		nodes[i] = stack->nodes[i];
	}
	free(stack->nodes);
	stack->nodes=nodes;
	stack->capacity = max;
}
void push(int value)
{
	if(stack->size == stack->capacity) {
		if(is_enable_resize){
			resize(stack->capacity *2 );
			stack->nodes[stack->size++]=value;
		}
		else {
			free_stack();
			die("Stack is full!\n");
		}
	} else {
		stack->nodes[stack->size++]=value;
	}
}
void pop()
{
	if(stack->size==0){
		free_stack();
		die("Stack is empty!\n");
	} else {
		--stack->size;
	}
}
int main(int argc, char *argv[])
{
	stack = new();
	printf("Initialize...\n");
	for(int i=0;i<stack->size;++i)
		printf("%3d",stack->nodes[i]);
	puts("");
	for(int i=0;i<INSERT_SIZE;++i)
		push(i);
	printf("After excuting push function...\n");
	for(int i=0;i<stack->size;++i)
		printf("%3d",stack->nodes[i]);
	puts("");
	printf("After excuting pop function...\n");
	pop();
	for(int i=0;i<stack->size;++i)
		printf("%3d",stack->nodes[i]);
	puts("");
	free_stack();
	return 0;
}
