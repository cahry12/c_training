#include<stdio.h>
#include<stdlib.h>
#include<stdint.h>
enum
{
	summer,
	winter
};
typedef struct EXAMPLE21
{
	int data;
	struct EXAMPLE21 *next;
}EX21;
//call by value
void fun1(int value)
{
	value++;
	printf("value=%d\n",value);
}
//call by reference
/*
void fun2(int &value)
{
	value++;
	printf("Value=%d\n",value);
}*/
//call by address
void fun3(int *value)
{
	(*value)++;
	printf("value=%d\n",*value);
}
int main(int argc,char *argv[])
{
	printf("INT(8)_MIN=%d,INT(8)_MAX=%d\n",INT8_MIN,INT8_MAX);
	printf("INT(16)_MIN=%d,INT(16)_MAX=%d\n",INT16_MIN,INT16_MAX);
	printf("INT(32)_MIN=%d,INT(32)_MAX=%d\n",INT32_MIN,INT32_MAX);
	printf("INT(64)_MIN=%lld,INT(64)_MAX=%lld\n",INT64_MIN,INT64_MAX);
	printf("INT(8)_MAX=%u\n",UINT8_MAX);
	printf("INT(16)_MAX=%u\n",UINT16_MAX);
	printf("INT(32)_MAX=%u\n",UINT32_MAX);
	printf("INT(64)_MAX=%llu\n",UINT64_MAX);
/*
	printf("%INT_LEAST(8)_MIN=%d, INT_LEAST(8)_MAX=%d\n",INT_LEAST8_MIN,INT_LEAST8_MAX);
	printf("%INT_LEAST(16)_MIN=%d, INT_LEAST(16)_MAX=%d\n",INT_LEAST16_MIN,INT_LEAST16_MAX);
	printf("%INT_LEAST(32)_MIN=%d, INT_LEAST(32)_MAX=%d\n",INT_LEAST32_MIN,INT_LEAST32_MAX);
	printf("%INT_LEAST(64)_MIN=%lld, INT_LEAST(64)_MAX=%lld\n",INT_LEAST64_MIN,INT_LEAST64_MAX);
*/
	void *nothing;
	char ch_ex;
	float ft_ex;
	int int_ex;
	short sh_ex;
	long lg_ex;
	unsigned un_ex;
	signed sign_ex;
	printf("[Size] void*=%lu,char=%lu,float=%lu,int=%lu,short=%lu,long=%lu,unsigned=%lu,signed=%lu\n",sizeof(nothing),
		sizeof(ch_ex), sizeof(ft_ex), sizeof(int_ex), sizeof(sh_ex), sizeof(lg_ex), sizeof(un_ex), sizeof(sign_ex));
	const int ci=0;
	volatile int vi=0;
	register int ri=0;
	printf("ci=%d,vi=%d,ri=%d\n",ci,vi++,ri++);//ci++ would compile error!

	printf("SIZE_MAX=%llu\n",SIZE_MAX);//SIZE_MAX==UINT(64)_MAX


	int operator=1;
	int op1=5,op2=3;
	// available operator
	printf("binary operator(ex: 1+2=%d), unary operator(ex:-1=%d), prefix operator(ex: ++operator=%d), postfix operator(ex: operator++=%d), ternary(ex: op1>op2:op1:op2=%d)\n",1+2,-1,++operator,operator++,op1>op2?op1:op2);
	
	EX21 *ptr,nptr;
	ptr=malloc(sizeof(EX21));
	ptr->data=20;
	nptr.data=50;
	printf("ptr's data=%d, nptr's data=%d\n",ptr->data,nptr.data);

	int a=5;
	printf("Before call function1, a's value is %d\n",a);
	fun1(a);
	printf("After call function1, a's value is %d\n",a);
	//printf("Before call function2, a's value is %d\n",a);
	//fun2(a);
	//printf("After call function1, a's value is %d\n",a);
	printf("Before call function3, a's value is %d\n",a);
	fun3(&a);
	printf("After call function3, a's value is %d\n",a);
	return 0;
}
