#include "minunit.h"
#include <lcthw/list.h>
#include <assert.h>

static List *list = NULL;
static List *front =NULL, *back=NULL;
char *test1 = "test1 data";
char *test2 = "test2 data";
char *test3 = "test3 data";


char *test_create()
{
    list = List_create();
    mu_assert(list != NULL, "Failed to create list.");

    return NULL;
}


char *test_destroy()
{
    List_clear_destroy(list);

    return NULL;

}


char *test_push_pop()
{
    List_push(list, test1);
    mu_assert(List_last(list) == test1, "Wrong last value.");

    List_push(list, test2);
    mu_assert(List_last(list) == test2, "Wrong last value");

    List_push(list, test3);
    mu_assert(List_last(list) == test3, "Wrong last value.");
    mu_assert(List_count(list) == 3, "Wrong count on push.");

    char *val = List_pop(list);
    mu_assert(val == test3, "Wrong value on pop.");

    val = List_pop(list);
    mu_assert(val == test2, "Wrong value on pop.");

    val = List_pop(list);
    mu_assert(val == test1, "Wrong value on pop.");
    mu_assert(List_count(list) == 0, "Wrong count after pop.");

    return NULL;
}

char *test_unshift()
{
    List_unshift(list, test1);
    mu_assert(List_first(list) == test1, "Wrong first value.");

    List_unshift(list, test2);
    mu_assert(List_first(list) == test2, "Wrong first value");

    List_unshift(list, test3);
    mu_assert(List_first(list) == test3, "Wrong last value.");
    mu_assert(List_count(list) == 3, "Wrong count on unshift.");

    return NULL;
}

char *test_remove()
{
    // we only need to test the middle remove case since push/shift 
    // already tests the other cases

    char *val = List_remove(list, list->first->next);
    mu_assert(val == test2, "Wrong removed element.");
    mu_assert(List_count(list) == 2, "Wrong count after remove.");
    mu_assert(List_first(list) == test3, "Wrong first after remove.");
    mu_assert(List_last(list) == test1, "Wrong last after remove.");

    return NULL;
}


char *test_shift()
{
    mu_assert(List_count(list) != 0, "Wrong count before shift.");

    char *val = List_shift(list);
    mu_assert(val == test3, "Wrong value on shift.");

    val = List_shift(list);
    mu_assert(val == test1, "Wrong value on shift.");
    mu_assert(List_count(list) == 0, "Wrong count after shift.");

    return NULL;
}

char *test_copy()
{
	mu_assert(List_count(list) !=0,"Wrong count before copy.");
	List *cpy_list=NULL;
	cpy_list=List_copy(list);
	mu_assert(cpy_list!=NULL,"copy list to cpy_lsit failed!");
	mu_assert(List_count(list) !=0,"Wrong count after copying.(For list)");
	mu_assert(List_count(cpy_list) !=0,"Wrong count after copying.(For cpy_list)");
	return NULL;
}
char *test_split()
{
	mu_assert(List_count(list)!=0,"Wrong count before split.");
	front=List_create();
	back=List_create();
	mu_assert(front!=NULL,"split list into front failed!");
	mu_assert(back!=NULL,"split list into back failed!");
	List_split(list,&front,&back);
	mu_assert(List_count(front)!=0,"Wrong count with front after splitting.");
	mu_assert(List_count(back)!=0,"Wrong count with back after splitting.");
	return NULL;
}
char *test_join()
{
	List_join(front,back,&list);
	if(front)
		free(front);
	front=NULL;
	mu_assert(front==NULL,"front is not NULL!");
	if(back)
		free(back);
	back=NULL;
	mu_assert(back==NULL,"back is not NULL!");
	return NULL;
}
char *all_tests() {
    mu_suite_start();

    mu_run_test(test_create);
    mu_run_test(test_push_pop);
    mu_run_test(test_unshift);
    mu_run_test(test_copy);
    mu_run_test(test_split);
    mu_run_test(test_join);
    mu_run_test(test_remove);
    mu_run_test(test_shift);
    mu_run_test(test_destroy);

    return NULL;
}

RUN_TESTS(all_tests);
