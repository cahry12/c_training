#include <lcthw/list.h>
#include <lcthw/dbg.h>
#include <assert.h>
List *List_create()
{
    return calloc(1, sizeof(List));
}

void List_destroy(List *list)
{

    assert(list !=NULL);
    LIST_FOREACH(list, first, next, cur) {
        if(cur->prev) {
            free(cur->prev);
        }
    }

    if(list->last)	//extra check to make list's contents always correct.
    	free(list->last);
    list->last=NULL;
    if(list->first)
	free(list->first);
    list->first=NULL;
    if(list)		//extra check to make list's contents always correct.
    	free(list);
    list=NULL;
}


void List_clear(List *list)
{
    assert(list !=NULL);
    LIST_FOREACH(list, first, next, cur) {
	if(cur->value) //extra check to make list-s contents always correct.
        	free(cur->value);
    }
}


void List_clear_destroy(List *list)
{
    assert(list !=NULL);
    List_clear(list);
    List_destroy(list);
}


void List_push(List *list, void *value)
{
    assert(list !=NULL);
    ListNode *node = calloc(1, sizeof(ListNode));
    check_mem(node);

    node->value = value;

    if(list->last == NULL) {
        list->first = node;
        list->last = node;
    } else {
        list->last->next = node;
        node->prev = list->last;
        list->last = node;
    }

    list->count++;

error:
    return;
}

void *List_pop(List *list)
{
    assert(list !=NULL);
    ListNode *node = list->last;
    return node != NULL ? List_remove(list, node) : NULL;
}

void List_unshift(List *list, void *value)
{
    assert(list !=NULL);
    ListNode *node = calloc(1, sizeof(ListNode));
    check_mem(node);

    node->value = value;

    if(list->first == NULL) {
        list->first = node;
        list->last = node;
    } else {
        node->next = list->first;
        list->first->prev = node;
        list->first = node;
    }

    list->count++;

error:
    return;
}

void *List_shift(List *list)
{
    assert(list !=NULL);
    ListNode *node = list->first;
    return node != NULL ? List_remove(list, node) : NULL;
}

void *List_remove(List *list, ListNode *node)
{
    assert(list !=NULL);
    void *result = NULL;

    check(list->first && list->last, "List is empty.");
    check(node, "node can't be NULL");

    if(node == list->first && node == list->last) { //Only one element!
        list->first = NULL;
        list->last = NULL;
    } else if(node == list->first) { // remove first node
        list->first = node->next;
        check(list->first != NULL, "Invalid list, somehow got a first that is NULL.");
        list->first->prev = NULL;
    } else if (node == list->last) { // remove last node
        list->last = node->prev;
        check(list->last != NULL, "Invalid list, somehow got a next that is NULL.");
        list->last->next = NULL;
    } else { //remove intermediate node
        ListNode *after = node->next;
        ListNode *before = node->prev;
        after->prev = before;
        before->next = after;
    }

    list->count--;
    result = node->value;
    free(node);

error:
    return result;
}

List* List_copy(List *list)
{
	assert(list!=NULL);
	List *cpy_list = List_create();
	cpy_list->first= cpy_list->last = NULL;
	ListNode *old_node, *node;
	LIST_FOREACH(list,first,next,cur) {
		node = calloc(1, sizeof(ListNode));
		check_mem(node);
		if(cpy_list->first==NULL){
			cpy_list->first=node;
			node->prev=NULL;
		} else {
			old_node->next=node;
			node->prev=old_node;
		}
		node->next=NULL;
		node->value=cur->value;
		old_node=node;
		if(cur->next==NULL)
			cpy_list->last=node;
	}
	cpy_list->count=list->count;
	//Testing... print
	printf("Print out cpy_list's elements...\n");
	ListNode *front_pt=cpy_list->first;
	while(front_pt){
		printf("%s\n",front_pt->value);
		front_pt=front_pt->next;
	}
	puts("");
	return cpy_list;
error:
	return cpy_list;
}/*
List* List_copy(List *list)
{
	assert(list!=NULL);
	List *cpy_list = List_create();

	cpy_list->first=list->first;
	LIST_FOREACH(list,first,next,cur) {
		ListNode *node = calloc(1, sizeof(ListNode));
		check_mem(node);
		node->prev=cur->prev;
		node->next=cur->next;
		node->value=cur->value;
	}
	cpy_list->last=list->last;
	cpy_list->count=list->count;
	//Testing... print
	printf("Print out cpy_list's elements...\n");
	ListNode *front_pt=cpy_list->first;
	while(front_pt){
		printf("%s\n",front_pt->value);
		front_pt=front_pt->next;
	}
	puts("");
	return cpy_list;
error:
	return cpy_list;
}*/
/*
List* List_copy(List *list)
{
	assert(list!=NULL);
	List *cpy_list = List_create();

	cpy_list->first=list->first;
	ListNode *cpy_list_cur=list->first;
	LIST_FOREACH(list,first,next,cur) {
		cpy_list_cur->prev=cur->prev;
		cpy_list_cur->next=cur->next;
		cpy_list_cur->value=cur->value;
		cpy_list_cur=cur->next;
	}
	cpy_list->last=list->last;
	cpy_list->count=list->count;
	return cpy_list;
}*/
void List_split(List *list, List **front, List **back)
{
	ListNode *front_tail_cur=NULL;
	ListNode *one_jmp=list->first;
	ListNode *two_jmp=list->first;
	while(two_jmp){
		front_tail_cur=one_jmp;
		one_jmp=one_jmp->next; //jump 一次
		two_jmp=(two_jmp->next?two_jmp->next->next:NULL); //直接jump 兩次
		(*front)->count++;
	}
	one_jmp->prev=NULL;
	//以{1,3,7}來說, one_jmp最後會於while-loop結束後指向7而front_tail_cur為指向3
	if(front_tail_cur->next!=NULL)//設定front的tail為null(1->3->null)
		front_tail_cur->next=NULL;
	(*front)->first=list->first;//設定front pointer
	(*front)->last=front_tail_cur;
	(*back)->first=one_jmp;//設定back pointer
	(*back)->last=list->last;
	(*back)->count=List_count(list)-List_count(*front);
	
	//Testing... print
	printf("Print out front's elements...\n");
	ListNode *front_pt=(*front)->first;
	while(front_pt){
		printf("%s\n",front_pt->value);
		front_pt=front_pt->next;
	}
	puts("");
	printf("Print out back's elements...\n");
	ListNode *back_pt=(*back)->first;
	while(back_pt){
		printf("%s\n",back_pt->value);
		back_pt=back_pt->next;
	}
	return;
}
void List_join(List *front, List *back,List **list)
{
	ListNode *front_tail_cur=front->last;
	front_tail_cur->next=back->first;
	ListNode *back_head_cur=back->first;
	back_head_cur->prev=front->last;
	(*list)->first=front->first;
	(*list)->last=back->last;
	(*list)->count=List_count(front)+List_count(back);
	return;
}
