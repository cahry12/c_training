#include<stdio.h>
#include<errno.h>
#include<stdlib.h>
//pass by value
void fun1(int value)
{
	printf("CALL FUNCTION1,value=%d\n",++value);
}
//pass by reference
void fun2(int &value)
{
	printf("CALL FUNCTION,value=%d\n",++value);
}
int main(int argc, char *argv[])
{
	int value=1;
	printf("Before calling function1, value=%d\n",value);
	fun1(value);
	printf("After calling function1, value=%d\n",value);

	printf("Before calling function2, value=%d\n",value);
	fun2(value);
	printf("After calling function2, value=%d\n",value);

	return 0;
}
