#include<stdio.h>
#include<stdlib.h>
#ifdef DEBUG
#define DEBUG
#endif
int main(int argc,char *argv[])
{
	float age = 2.0f;
	char str[]="STRING";
	char *str1=str;
	int height = 72;
	double e=0.00001;
	printf("[%%a FORMAT] pi=%a.\n", 3.14);
	printf("[%%A FORMAT] pi=%A.\n", 3.14);
	printf("[%%s FORMAT] str=%s,str1=%s\n",str,str1);
	//str="CHANG_STRING";  //compile error! due to the constant!
	printf("[%%s FORMAT] str=%s,str1=%s\n",str,str1);

	printf("[%%f FORMAT] age=%f, e=%f\n",age,e);
	printf("[%%g FORMAT] age=%g, e=%g\n",age,e);
	printf("[%%G FORMAT] age=%G, e=%G\n",age,e);
	printf("[%%e FORMAT] age=%e, e=%e\n",age,e);
	printf("[%%E FORMAT] age=%E, e=%E\n",age,e);
	
	printf("[%%d FORMAT] height=%d\n",height);
	printf("[%%i FORMAT] height=%i\n",height);
	printf("[%%u FORMAT] height=%u\n",height);
	printf("[%%o FORMAT] height=%o\n",height);
	printf("[%%x FORMAT] height=%x\n",height);
	printf("[%%X FORMAT] height=%X\n",height);
	printf("[%%p FORMAT] height=%p\n",height);//compiler warning! due to the reason that height is integer!
	printf("[%%p FORMAT_2] height=%p\n",&height);

	printf("I am %d inches tall.\n",height);

	return 0;
}

