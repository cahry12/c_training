#include<stdio.h>
#include<stdlib.h>
#include<assert.h>
#include<string.h>
typedef struct Person {
	char *name;
	int age;
	int height;
	int weight;
}PERSON;
PERSON *Person_create(char *name,int age, int height, int weight)
{
	PERSON *who = (PERSON*)malloc(sizeof(PERSON));
	assert(who!=NULL);

	who->name = strdup(name);
	who->age = age;
	who->height = height;
	who->weight = weight;
	return who;
}
void Person_destory(PERSON *who)
{
	assert(who != NULL);
	free(who->name);
	free(who);
}
void Person_print(PERSON *who)
{
	printf("Name: %s\n",who->name);
	printf("\tAge: %d\n",who->age);
	printf("\tHeight: %d\n",who->height);
	printf("\tWeight: %d\n",who->weight);
}
int main(int argc, char *argv[])
{
	PERSON *joe = Person_create("Joe Alex", 32 , 64, 140);
	PERSON *frank = Person_create("Frank Blank", 20, 72, 180);
	printf("Joe is at memory location: %p\n",joe);
	printf("sizeof(Joe)=%lu,sizeof(frank)=%lu,sizeof(Person)=%lu\n",sizeof(*joe),sizeof(*frank),sizeof(PERSON));
	printf("sizeof(Joe.name)=%lu,sizeof(Joe.age)=%lu,sizeof(Joe.height)=%lu,sizeof(Joe.weight)=%lu\n",sizeof(joe->name) \
			,sizeof(joe->age),sizeof(joe->height),sizeof(joe->weight));
	printf("sizeof(frank.name)=%lu,sizeof(frank.age)=%lu,sizeof(frank.height)=%lu,sizeof(frank.weight)=%lu\n",sizeof(frank->name) \
			,sizeof(frank->age),sizeof(frank->height),sizeof(frank->weight));
	Person_print(joe);
	printf("Frank is at memory location: %p\n",frank);
	Person_print(frank);
	return 0;
}
