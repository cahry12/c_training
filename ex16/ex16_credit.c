#include<stdio.h>
#include<stdlib.h>
#include<assert.h>
#include<string.h>
typedef struct Person {
	char *name;
	int age;
	int height;
	int weight;
}PERSON;
int main(int argc, char *argv[])
{
	PERSON joe = { "Joe Alex", 32 , 64, 140};
	PERSON frank = { "Frank Blank", 20, 72, 180};
	printf("Joe is at memory location: %p\n",&joe);
	printf("Name: %s\n\tAge: %d\n\tHeight: %d\n\tWeight: %d\n",joe.name,joe.age,joe.height,joe.weight);
	printf("Frank is at memory location: %p\n",&frank);
	printf("Name: %s\n\tAge: %d\n\tHeight: %d\n\tWeight: %d\n",frank.name,frank.age,frank.height,frank.weight);
	return 0;
}
