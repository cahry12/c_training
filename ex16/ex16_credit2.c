#include<stdio.h>
#include<stdlib.h>
#include<assert.h>
#include<string.h>
typedef struct Person {
	char *name;
	int age;
	int height;
	int weight;
}PERSON;
void Person_print(PERSON who)
{
	printf("Name: %s\n",who.name);
	printf("\tAge: %d\n",who.age);
	printf("\tHeight: %d\n",who.height);
	printf("\tWeight: %d\n",who.weight);
}
int main(int argc, char *argv[])
{
	PERSON joe = {"Joe Alex", 32 , 64, 140};
	PERSON frank = {"Frank Blank", 20, 72, 180};
	printf("Joe is at memory location: %p\n",&joe);
	Person_print(joe);
	printf("Frank is at memory location: %p\n",&frank);
	Person_print(frank);
	return 0;
}
