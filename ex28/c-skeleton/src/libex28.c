#include <stdio.h>
#include <stdlib.h>
#include <string.h>
//###- A part of string function(begin) -###
size_t my_strlen(const char *src)
{
	size_t len=0;
	for(size_t i=0;src[i]!='\0';++i)++len;
	return len;
}
char *my_strcpy(char *dst,const char* src)
{
	size_t i=0;
	while(src[i]){
		dst[i]=src[i];//or dst[i++]=src[i];
		i++;
	}
	dst[i]='\0';
	return dst;
}
char *my_strncpy(char *dst,const char* src,size_t len)
{
	size_t i=0;
	while(src[i]&&i<len){
		dst[i]=src[i];
		i++;
	}
	dst[i]='\0';
	return dst;
}
int strcmp(const char* s1, const char* s2)
{
	for(;*s1==*s2;s1++,s2++)
	{
		if(*s1=='\0')
			return 0;
	}
	return ((*(unsigned char *)s1 < *(unsigned char *)s2) ? -1: +1);
}
char *my_strdup(const char * src)
{
	size_t len=1+my_strlen(src);
	char *p=malloc(len);
	return p ? memcpy(p,src,len):NULL;
}
//###- A part of string function(end) -###
