#include<stdio.h>
#include<stdlib.h>
#define MAX 10
int main(int argc, char *argv[]){
	int ages[] = {23, 43, 12, 89, 2};
	char *names[] = {
		"Alan", "Frank",
		"Mary", "John", "Lisa"
	};
	int count = sizeof(ages)/sizeof(int);
	int i=0;
	while(i<count){
		printf("%s has %d years alive. \n",names[i],ages[i++]);
	}	
	printf("---\n");
	int *cur_age = ages;
	char **cur_name=names;
	i=0;
	while(i<count){
		printf("%s is %d years old.\n",*(cur_name+i),*(cur_age+i++));
	}
	printf("---\n");
	i=0;
	while(i<count){
		printf("%s is %d years old again.\n",
				cur_name[i], cur_age[i++]);
	}
	printf("---\n");
	cur_name=names,cur_age=ages;
	while((cur_age-ages)<count){
		printf("%s lived %d years so far.\n",*cur_name++, *cur_age++);
	}
	return 0;
}
