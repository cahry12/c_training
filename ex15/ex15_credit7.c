#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#define MAX 10
void printFirst(int ages[], char *names[], int count)
{
	for(int i=0;i<count;i++)
		printf("%s has %d years alive. \n",names[i],ages[i]);
	puts("");
}
void printSecond(int *ages, char **names, int count)
{
	for(int i=0;i<count;i++)
		printf("%s has %d years alive. \n",*(names+i),*(ages+i));
	puts("");
}
void printThird(int *ages, char **names, int count)
{
	for(int i=0;i<count;i++)
		printf("%s is %d old again. \n",names[i],ages[i]);
	puts("");
}
void printFouth(int *ages, char **names, int *cur_age, char **cur_name, int count)
{
	for(cur_name=names, cur_age=ages ; (cur_age-ages)<count ; cur_name++,cur_age++)
	{
		printf("%s lived %d years so far.\n",*cur_name, *cur_age);
	}
	puts("");
}
int main(int argc, char *argv[]){
	int ages[] = {23, 43, 12, 89, 2};
	char *names[] = {
		"Alan", "Frank",
		"Mary", "John", "Lisa"
	};
	int count = sizeof(ages)/sizeof(int);
	int i=0;
	int *cur_age = ages;
	char **cur_name=names;
	printFirst(ages,names,count);
	printSecond(ages,names,count);
	printThird(cur_age,cur_name,count);
	printFouth(ages,names,cur_age,cur_name,count);
	return 0;
}
