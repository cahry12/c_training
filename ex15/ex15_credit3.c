#include<stdio.h>
#include<stdlib.h>
#ifdef DEBUG
#define DEBUG
#endif
#define getNumberInArrar(arr) (sizeof(arr),sizeof(arr[0]))
int main(int argc,char *argv[])
{
	int i=0;
	while( i < argc) {
		printf("arg %d: %s\n",i,argv[i]);
		i++;
	}
	char *states[] = {
		"California", "Oregon",
		"Washington", "Texas"
	};
	int num_states = 4;
	char **states_pointer = states;
	//int i=0;//compile error!
	int states_num[]={10,30,50,80};
	int *cur_states_num=states_num;
	i=0;
	while( i < num_states) {
		printf("state %d: %s state's num=%d\n",i , *(states+i),*(cur_states_num+i));
		i++;
	}
	printf("\n\nAfter changing some element's value...\n");
	*(cur_states_num+1)=99;
	*(cur_states_num+3)=88;
	//*(cur_states_num+4)=77;//segmention fault!
	i=0;
	while( i < num_states) {
		printf("state %d: %s state's num=%d\n",i , *(states+i),*(cur_states_num+i));
		i++;
	}
	return 0;
}
