#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#define MAX 10
int main(int argc, char *argv[]){
	int ages[] = {23, 43, 12, 89, 2};
	char *names[] = {
		"Alan", "Frank",
		"Mary", "John", "Lisa"
	};
	int count = sizeof(ages)/sizeof(int);
	int *cur_age = ages;
	char **cur_name=names;
	for(cur_name=names, cur_age=ages ; (cur_age-ages)<count ; cur_name++,cur_age++)
	{
		printf("[cur_names] value: %s, address: %p.\n",*cur_name, cur_name);
	}
	puts("");
	for(cur_age=ages ; (cur_age-ages)<count ; cur_age++)
	{
		printf("[cur_age] value: %d, address: %p.\n",*cur_age, cur_age);
	}
	return 0;
}
