#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#define MAX 10
int main(int argc, char *argv[]){
	int ages[] = {23, 43, 12, 89, 2};
	char *names[] = {
		"Alan", "Frank",
		"Mary", "John", "Lisa"
	};
	int count = sizeof(ages)/sizeof(int);
	int i;
	for (i=count-1;i>=0;--i){
		printf("%s(address=%p) has %d(address=%p) years alive. \n",names[i], &names[i], ages[i], &ages[i]);
	}	
	printf("---\n");
	int *cur_age = ages,*cur_age_end;
	char **cur_name=names, **cur_name_end;
	for(i=count-1;i>=0;--i){
		printf("%s is %d years old.\n",*(cur_name+i),*(cur_age+i));
		if(i==count-1){
			cur_age_end = cur_age+i;
			cur_name_end = cur_name+i;
		}
	}
	printf("---\n");
	for(i=count-1;i>=0;--i){
		printf("%s is %d years old again.\n",
				cur_name[i], cur_age[i]);
	}
	printf("---\n");
	//method 1(Relative address)
	for(cur_name=names+count-1, cur_age=ages+count-1 ; cur_age>=ages ; cur_name--,cur_age--)
	{
		printf("%s lived %d years so far.\n",*cur_name, *cur_age);
	}
	printf("---\n");
	//method 2(Absolute address)
	for(cur_name=cur_name_end, cur_age=cur_age_end ; cur_age>=ages ; cur_name--,cur_age--)
	{
		printf("%s lived %d years so far.\n",*cur_name, *cur_age);
	}
	return 0;
}
