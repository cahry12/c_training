#include<stdio.h>
#include<stdlib.h>
int main(int argc, char *argv[]){
	int distance = 100;
	float power = 2.345f;
	double super_power = 56789.4532;
	char first_name[]="Shih-Ping";
	printf("You are %5d miles away.\n",distance);
	printf("You are %09d miles away.\n",distance);
	printf("You have %.3f levels of power.\n",power);
	printf("You have %3.2f awesome super powers.\n",super_power);
	printf("I have a last name %s.\n",first_name);
	printf("I have a last name %8s.\n",first_name);
	printf("I have a last name %.10s.\n",first_name);
	printf("I have a last name %-6s.\n",first_name);
	printf("I have a last name %-20s.\n",first_name);
	return 0;
}
