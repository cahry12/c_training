


public class ex10_break {
	public static void main(String[] args){
		int i=0;
		for ( i=0; i < args.length ; ++i){
			System.out.printf("arg %d: %s\n",i,args[i]);
		}

		char[][] states = new char[][] 
		{"California".toCharArray(), "Oregon".toCharArray(), "Washington".toCharArray(), "Texas".toCharArray()};
		int num_states = 4;
		for( i=0;i<num_states;i++){
			System.out.printf("state %d: %s\n",i,states[i]);
		}
	}
}
