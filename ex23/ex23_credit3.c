#include <stdio.h>
#include <string.h>
#include "dbg.h"
#include <time.h>
#define EXCUTE_TIME 10000
int normal_copy(char *from, char *to, int count)
{
    int i = 0;

    for(i = 0; i < count; i++) {
        to[i] = from[i];
    }

    return i;
}

int duffs_device(char *from, char *to, int count)
{
    {
        int n = (count + 7) / 8;

        switch(count % 8) {
            case 0: do { *to++ = *from++;
                        case 7: *to++ = *from++;
                        case 6: *to++ = *from++;
                        case 5: *to++ = *from++;
                        case 4: *to++ = *from++;
                        case 3: *to++ = *from++;
                        case 2: *to++ = *from++;
                        case 1: *to++ = *from++;
                    } while(--n > 0);
        }
    }

    return count;
}

int zeds_device(char *from, char *to, int count)
{
    {
        int n = (count + 7) / 8;

        switch(count % 8) {
            case 0:
            again: *to++ = *from++;

            case 7: *to++ = *from++;
            case 6: *to++ = *from++;
            case 5: *to++ = *from++;
            case 4: *to++ = *from++;
            case 3: *to++ = *from++;
            case 2: *to++ = *from++;
            case 1: *to++ = *from++;
                    if(--n > 0) goto again;
        }
    }

    return count;
}

int valid_copy(char *data, int count, char expects)
{
    int i = 0;
    for(i = 0; i < count; i++) {
        if(data[i] != expects) {
            log_err("[%d] %c != %c", i, data[i], expects);
            return 0;
        }
    }

    return 1;
}


int main(int argc, char *argv[])
{
    char from[1000] = {'a'};
    char to[1000] = {'c'};
    double avg_diff;
    int rc = 0;
    int i;
    clock_t begin,end;
    // setup the from to have some stuff
    memset(from, 'x', 1000);
    // set it to a failure mode
    memset(to, 'y', 1000);
    check(valid_copy(to, 1000, 'y'), "Not initialized right.");

    // use normal copy to 
    for(i=0,avg_diff=0.0;i<EXCUTE_TIME;++i){
    	begin=clock();
    	rc = normal_copy(from, to, 1000);
    	end=clock();
    	avg_diff+=(double)end-begin;
    }
    //printf("The time period of normal_copy is %f sec\n",(double)(end-begin)/(double)CLOCKS_PER_SEC);
    printf("The time period of normal_copy is %f ms(average run 10000 times)\n",avg_diff/EXCUTE_TIME);
    
    check(rc == 1000, "Normal copy failed: %d", rc);
    check(valid_copy(to, 1000, 'x'), "Normal copy failed.");

    // reset
    memset(to, 'y', 1000);

    // duffs version
    for(i=0,avg_diff=0.0;i<EXCUTE_TIME;++i){
    	begin=clock();
   	rc = duffs_device(from, to, 1000);
    	end=clock();
	avg_diff+=(double)end-begin;
    }
    //printf("The time period of duffs_device is %f sec\n",(double)(end-begin)/(double)CLOCKS_PER_SEC);
    printf("The time period of duffs_device is %f ms(average run 10000 times)\n",avg_diff/EXCUTE_TIME);
    check(rc == 1000, "Duff's device failed: %d", rc);
    check(valid_copy(to, 1000, 'x'), "Duff's device failed copy.");

    // reset
    memset(to, 'y', 1000);

    // my version
    for(i=0,avg_diff=0.0;i<EXCUTE_TIME;++i){
    	begin=clock();
    	rc = zeds_device(from, to, 1000);
    	end=clock();
    	avg_diff+=(double)end-begin;
    }
    printf("The time period of zeds_device is %f ms(average run 10000 times)\n", avg_diff/EXCUTE_TIME);
    //printf("The time period of zeds_device is %f sec\n",(double)(end-begin)/(double)CLOCKS_PER_SEC);
    check(rc == 1000, "Zed's device failed: %d", rc);
    check(valid_copy(to, 1000, 'x'), "Zed's device failed copy.");

    return 0;
error:
    return 1;
}
