	.section	__TEXT,__text,regular,pure_instructions
	.macosx_version_min 10, 10
	.globl	_normal_copy
	.align	4, 0x90
_normal_copy:                           ## @normal_copy
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp0:
	.cfi_def_cfa_offset 16
Ltmp1:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp2:
	.cfi_def_cfa_register %rbp
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movl	%edx, -20(%rbp)
	movl	$0, -24(%rbp)
	movl	$0, -24(%rbp)
LBB0_1:                                 ## =>This Inner Loop Header: Depth=1
	movl	-24(%rbp), %eax
	cmpl	-20(%rbp), %eax
	jge	LBB0_4
## BB#2:                                ##   in Loop: Header=BB0_1 Depth=1
	movslq	-24(%rbp), %rax
	movq	-8(%rbp), %rcx
	movb	(%rcx,%rax), %dl
	movslq	-24(%rbp), %rax
	movq	-16(%rbp), %rcx
	movb	%dl, (%rcx,%rax)
## BB#3:                                ##   in Loop: Header=BB0_1 Depth=1
	movl	-24(%rbp), %eax
	addl	$1, %eax
	movl	%eax, -24(%rbp)
	jmp	LBB0_1
LBB0_4:
	movl	-24(%rbp), %eax
	popq	%rbp
	retq
	.cfi_endproc

	.globl	_duffs_device
	.align	4, 0x90
_duffs_device:                          ## @duffs_device
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp3:
	.cfi_def_cfa_offset 16
Ltmp4:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp5:
	.cfi_def_cfa_register %rbp
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movl	%edx, -20(%rbp)
	movl	%edx, %eax
	addl	$7, %eax
	movl	%edx, %esi
	sarl	$31, %eax
	shrl	$29, %eax
	movl	%eax, %edi
	leal	7(%rsi,%rdi), %eax
	sarl	$3, %eax
	movl	%eax, -24(%rbp)
	movl	-20(%rbp), %eax
	movl	%eax, %edx
	sarl	$31, %edx
	shrl	$29, %edx
	movl	%eax, %ecx
	addl	%edx, %ecx
	andl	$-8, %ecx
	subl	%ecx, %eax
	movl	%eax, %esi
	subl	$7, %eax
	movq	%rsi, -32(%rbp)         ## 8-byte Spill
	movl	%eax, -36(%rbp)         ## 4-byte Spill
	ja	LBB1_12
## BB#13:
	leaq	LJTI1_0(%rip), %rax
	movq	-32(%rbp), %rcx         ## 8-byte Reload
	movslq	(%rax,%rcx,4), %rdx
	addq	%rax, %rdx
	jmpq	*%rdx
LBB1_1:
	jmp	LBB1_2
LBB1_2:
	movq	-8(%rbp), %rax
	movq	%rax, %rcx
	addq	$1, %rcx
	movq	%rcx, -8(%rbp)
	movb	(%rax), %dl
	movq	-16(%rbp), %rax
	movq	%rax, %rcx
	addq	$1, %rcx
	movq	%rcx, -16(%rbp)
	movb	%dl, (%rax)
LBB1_3:
	movq	-8(%rbp), %rax
	movq	%rax, %rcx
	addq	$1, %rcx
	movq	%rcx, -8(%rbp)
	movb	(%rax), %dl
	movq	-16(%rbp), %rax
	movq	%rax, %rcx
	addq	$1, %rcx
	movq	%rcx, -16(%rbp)
	movb	%dl, (%rax)
LBB1_4:
	movq	-8(%rbp), %rax
	movq	%rax, %rcx
	addq	$1, %rcx
	movq	%rcx, -8(%rbp)
	movb	(%rax), %dl
	movq	-16(%rbp), %rax
	movq	%rax, %rcx
	addq	$1, %rcx
	movq	%rcx, -16(%rbp)
	movb	%dl, (%rax)
LBB1_5:
	movq	-8(%rbp), %rax
	movq	%rax, %rcx
	addq	$1, %rcx
	movq	%rcx, -8(%rbp)
	movb	(%rax), %dl
	movq	-16(%rbp), %rax
	movq	%rax, %rcx
	addq	$1, %rcx
	movq	%rcx, -16(%rbp)
	movb	%dl, (%rax)
LBB1_6:
	movq	-8(%rbp), %rax
	movq	%rax, %rcx
	addq	$1, %rcx
	movq	%rcx, -8(%rbp)
	movb	(%rax), %dl
	movq	-16(%rbp), %rax
	movq	%rax, %rcx
	addq	$1, %rcx
	movq	%rcx, -16(%rbp)
	movb	%dl, (%rax)
LBB1_7:
	movq	-8(%rbp), %rax
	movq	%rax, %rcx
	addq	$1, %rcx
	movq	%rcx, -8(%rbp)
	movb	(%rax), %dl
	movq	-16(%rbp), %rax
	movq	%rax, %rcx
	addq	$1, %rcx
	movq	%rcx, -16(%rbp)
	movb	%dl, (%rax)
LBB1_8:
	movq	-8(%rbp), %rax
	movq	%rax, %rcx
	addq	$1, %rcx
	movq	%rcx, -8(%rbp)
	movb	(%rax), %dl
	movq	-16(%rbp), %rax
	movq	%rax, %rcx
	addq	$1, %rcx
	movq	%rcx, -16(%rbp)
	movb	%dl, (%rax)
LBB1_9:
	movq	-8(%rbp), %rax
	movq	%rax, %rcx
	addq	$1, %rcx
	movq	%rcx, -8(%rbp)
	movb	(%rax), %dl
	movq	-16(%rbp), %rax
	movq	%rax, %rcx
	addq	$1, %rcx
	movq	%rcx, -16(%rbp)
	movb	%dl, (%rax)
## BB#10:
	movl	-24(%rbp), %eax
	addl	$4294967295, %eax       ## imm = 0xFFFFFFFF
	movl	%eax, -24(%rbp)
	cmpl	$0, %eax
	jg	LBB1_2
## BB#11:
	jmp	LBB1_12
LBB1_12:
	movl	-20(%rbp), %eax
	popq	%rbp
	retq
	.cfi_endproc
	.align	2, 0x90
L1_0_set_1 = LBB1_1-LJTI1_0
L1_0_set_9 = LBB1_9-LJTI1_0
L1_0_set_8 = LBB1_8-LJTI1_0
L1_0_set_7 = LBB1_7-LJTI1_0
L1_0_set_6 = LBB1_6-LJTI1_0
L1_0_set_5 = LBB1_5-LJTI1_0
L1_0_set_4 = LBB1_4-LJTI1_0
L1_0_set_3 = LBB1_3-LJTI1_0
LJTI1_0:
	.long	L1_0_set_1
	.long	L1_0_set_9
	.long	L1_0_set_8
	.long	L1_0_set_7
	.long	L1_0_set_6
	.long	L1_0_set_5
	.long	L1_0_set_4
	.long	L1_0_set_3

	.globl	_zeds_device
	.align	4, 0x90
_zeds_device:                           ## @zeds_device
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp6:
	.cfi_def_cfa_offset 16
Ltmp7:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp8:
	.cfi_def_cfa_register %rbp
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movl	%edx, -20(%rbp)
	movl	%edx, %eax
	addl	$7, %eax
	movl	%edx, %esi
	sarl	$31, %eax
	shrl	$29, %eax
	movl	%eax, %edi
	leal	7(%rsi,%rdi), %eax
	sarl	$3, %eax
	movl	%eax, -24(%rbp)
	movl	-20(%rbp), %eax
	movl	%eax, %edx
	sarl	$31, %edx
	shrl	$29, %edx
	movl	%eax, %ecx
	addl	%edx, %ecx
	andl	$-8, %ecx
	subl	%ecx, %eax
	movl	%eax, %esi
	subl	$7, %eax
	movq	%rsi, -32(%rbp)         ## 8-byte Spill
	movl	%eax, -36(%rbp)         ## 4-byte Spill
	ja	LBB2_12
## BB#13:
	leaq	LJTI2_0(%rip), %rax
	movq	-32(%rbp), %rcx         ## 8-byte Reload
	movslq	(%rax,%rcx,4), %rdx
	addq	%rax, %rdx
	jmpq	*%rdx
LBB2_1:
	jmp	LBB2_2
LBB2_2:
	movq	-8(%rbp), %rax
	movq	%rax, %rcx
	addq	$1, %rcx
	movq	%rcx, -8(%rbp)
	movb	(%rax), %dl
	movq	-16(%rbp), %rax
	movq	%rax, %rcx
	addq	$1, %rcx
	movq	%rcx, -16(%rbp)
	movb	%dl, (%rax)
LBB2_3:
	movq	-8(%rbp), %rax
	movq	%rax, %rcx
	addq	$1, %rcx
	movq	%rcx, -8(%rbp)
	movb	(%rax), %dl
	movq	-16(%rbp), %rax
	movq	%rax, %rcx
	addq	$1, %rcx
	movq	%rcx, -16(%rbp)
	movb	%dl, (%rax)
LBB2_4:
	movq	-8(%rbp), %rax
	movq	%rax, %rcx
	addq	$1, %rcx
	movq	%rcx, -8(%rbp)
	movb	(%rax), %dl
	movq	-16(%rbp), %rax
	movq	%rax, %rcx
	addq	$1, %rcx
	movq	%rcx, -16(%rbp)
	movb	%dl, (%rax)
LBB2_5:
	movq	-8(%rbp), %rax
	movq	%rax, %rcx
	addq	$1, %rcx
	movq	%rcx, -8(%rbp)
	movb	(%rax), %dl
	movq	-16(%rbp), %rax
	movq	%rax, %rcx
	addq	$1, %rcx
	movq	%rcx, -16(%rbp)
	movb	%dl, (%rax)
LBB2_6:
	movq	-8(%rbp), %rax
	movq	%rax, %rcx
	addq	$1, %rcx
	movq	%rcx, -8(%rbp)
	movb	(%rax), %dl
	movq	-16(%rbp), %rax
	movq	%rax, %rcx
	addq	$1, %rcx
	movq	%rcx, -16(%rbp)
	movb	%dl, (%rax)
LBB2_7:
	movq	-8(%rbp), %rax
	movq	%rax, %rcx
	addq	$1, %rcx
	movq	%rcx, -8(%rbp)
	movb	(%rax), %dl
	movq	-16(%rbp), %rax
	movq	%rax, %rcx
	addq	$1, %rcx
	movq	%rcx, -16(%rbp)
	movb	%dl, (%rax)
LBB2_8:
	movq	-8(%rbp), %rax
	movq	%rax, %rcx
	addq	$1, %rcx
	movq	%rcx, -8(%rbp)
	movb	(%rax), %dl
	movq	-16(%rbp), %rax
	movq	%rax, %rcx
	addq	$1, %rcx
	movq	%rcx, -16(%rbp)
	movb	%dl, (%rax)
LBB2_9:
	movq	-8(%rbp), %rax
	movq	%rax, %rcx
	addq	$1, %rcx
	movq	%rcx, -8(%rbp)
	movb	(%rax), %dl
	movq	-16(%rbp), %rax
	movq	%rax, %rcx
	addq	$1, %rcx
	movq	%rcx, -16(%rbp)
	movb	%dl, (%rax)
	movl	-24(%rbp), %esi
	addl	$4294967295, %esi       ## imm = 0xFFFFFFFF
	movl	%esi, -24(%rbp)
	cmpl	$0, %esi
	jle	LBB2_11
## BB#10:
	jmp	LBB2_2
LBB2_11:
	jmp	LBB2_12
LBB2_12:
	movl	-20(%rbp), %eax
	popq	%rbp
	retq
	.cfi_endproc
	.align	2, 0x90
L2_0_set_1 = LBB2_1-LJTI2_0
L2_0_set_9 = LBB2_9-LJTI2_0
L2_0_set_8 = LBB2_8-LJTI2_0
L2_0_set_7 = LBB2_7-LJTI2_0
L2_0_set_6 = LBB2_6-LJTI2_0
L2_0_set_5 = LBB2_5-LJTI2_0
L2_0_set_4 = LBB2_4-LJTI2_0
L2_0_set_3 = LBB2_3-LJTI2_0
LJTI2_0:
	.long	L2_0_set_1
	.long	L2_0_set_9
	.long	L2_0_set_8
	.long	L2_0_set_7
	.long	L2_0_set_6
	.long	L2_0_set_5
	.long	L2_0_set_4
	.long	L2_0_set_3

	.globl	_valid_copy
	.align	4, 0x90
_valid_copy:                            ## @valid_copy
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp9:
	.cfi_def_cfa_offset 16
Ltmp10:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp11:
	.cfi_def_cfa_register %rbp
	subq	$80, %rsp
	movb	%dl, %al
	movq	%rdi, -16(%rbp)
	movl	%esi, -20(%rbp)
	movb	%al, -21(%rbp)
	movl	$0, -28(%rbp)
	movl	$0, -28(%rbp)
LBB3_1:                                 ## =>This Inner Loop Header: Depth=1
	movl	-28(%rbp), %eax
	cmpl	-20(%rbp), %eax
	jge	LBB3_9
## BB#2:                                ##   in Loop: Header=BB3_1 Depth=1
	movslq	-28(%rbp), %rax
	movq	-16(%rbp), %rcx
	movsbl	(%rcx,%rax), %edx
	movsbl	-21(%rbp), %esi
	cmpl	%esi, %edx
	je	LBB3_7
## BB#3:
	movq	___stderrp@GOTPCREL(%rip), %rax
	movq	(%rax), %rdi
	movq	%rdi, -40(%rbp)         ## 8-byte Spill
	callq	___error
	cmpl	$0, (%rax)
	jne	LBB3_5
## BB#4:
	leaq	L_.str2(%rip), %rax
	movq	%rax, -48(%rbp)         ## 8-byte Spill
	jmp	LBB3_6
LBB3_5:
	callq	___error
	movl	(%rax), %edi
	callq	_strerror
	movq	%rax, -48(%rbp)         ## 8-byte Spill
LBB3_6:
	movq	-48(%rbp), %rax         ## 8-byte Reload
	leaq	L_.str(%rip), %rsi
	leaq	L_.str1(%rip), %rdx
	movl	$66, %ecx
	movl	-28(%rbp), %r9d
	movslq	-28(%rbp), %rdi
	movq	-16(%rbp), %r8
	movsbl	(%r8,%rdi), %r10d
	movsbl	-21(%rbp), %r11d
	movq	-40(%rbp), %rdi         ## 8-byte Reload
	movq	%rax, %r8
	movl	%r10d, (%rsp)
	movl	%r11d, 8(%rsp)
	movb	$0, %al
	callq	_fprintf
	movl	$0, -4(%rbp)
	movl	%eax, -52(%rbp)         ## 4-byte Spill
	jmp	LBB3_10
LBB3_7:                                 ##   in Loop: Header=BB3_1 Depth=1
	jmp	LBB3_8
LBB3_8:                                 ##   in Loop: Header=BB3_1 Depth=1
	movl	-28(%rbp), %eax
	addl	$1, %eax
	movl	%eax, -28(%rbp)
	jmp	LBB3_1
LBB3_9:
	movl	$1, -4(%rbp)
LBB3_10:
	movl	-4(%rbp), %eax
	addq	$80, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	_main
	.align	4, 0x90
_main:                                  ## @main
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp12:
	.cfi_def_cfa_offset 16
Ltmp13:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp14:
	.cfi_def_cfa_register %rbp
	pushq	%rbx
	subq	$2312, %rsp             ## imm = 0x908
Ltmp15:
	.cfi_offset %rbx, -24
	movl	$1000, %eax             ## imm = 0x3E8
	movl	$121, %ecx
	leaq	-2032(%rbp), %rdx
	movl	$1000, %r8d             ## imm = 0x3E8
	movl	%r8d, %r9d
	movl	$120, %r8d
	leaq	-1024(%rbp), %r10
	xorl	%r11d, %r11d
	movq	___stack_chk_guard@GOTPCREL(%rip), %rbx
	movq	(%rbx), %rbx
	movq	%rbx, -16(%rbp)
	movl	$0, -2036(%rbp)
	movl	%edi, -2040(%rbp)
	movq	%rsi, -2048(%rbp)
	movq	%r10, %rsi
	movq	%rsi, %rdi
	movl	%r11d, %esi
	movq	%rdx, -2064(%rbp)       ## 8-byte Spill
	movq	%r9, %rdx
	movl	%r11d, -2068(%rbp)      ## 4-byte Spill
	movq	%r10, -2080(%rbp)       ## 8-byte Spill
	movl	%eax, -2084(%rbp)       ## 4-byte Spill
	movl	%ecx, -2088(%rbp)       ## 4-byte Spill
	movl	%r8d, -2092(%rbp)       ## 4-byte Spill
	movq	%r9, -2104(%rbp)        ## 8-byte Spill
	callq	_memset
	movb	$97, -1024(%rbp)
	movq	-2064(%rbp), %rdx       ## 8-byte Reload
	movq	%rdx, %rdi
	movl	-2068(%rbp), %esi       ## 4-byte Reload
	movq	-2104(%rbp), %rdx       ## 8-byte Reload
	callq	_memset
	movb	$99, -2032(%rbp)
	movl	$0, -2052(%rbp)
	movq	-2080(%rbp), %rdx       ## 8-byte Reload
	movq	%rdx, %rdi
	movl	-2092(%rbp), %esi       ## 4-byte Reload
	movq	-2104(%rbp), %rdx       ## 8-byte Reload
	callq	_memset
	movq	-2064(%rbp), %rdx       ## 8-byte Reload
	movq	%rdx, %rdi
	movl	-2088(%rbp), %esi       ## 4-byte Reload
	movq	-2104(%rbp), %rdx       ## 8-byte Reload
	callq	_memset
	movq	-2064(%rbp), %rdi       ## 8-byte Reload
	movl	-2084(%rbp), %esi       ## 4-byte Reload
	movl	-2088(%rbp), %edx       ## 4-byte Reload
	callq	_valid_copy
	cmpl	$0, %eax
	jne	LBB4_5
## BB#1:
	movq	___stderrp@GOTPCREL(%rip), %rax
	movq	(%rax), %rdi
	movq	%rdi, -2112(%rbp)       ## 8-byte Spill
	callq	___error
	cmpl	$0, (%rax)
	jne	LBB4_3
## BB#2:
	leaq	L_.str2(%rip), %rax
	movq	%rax, -2120(%rbp)       ## 8-byte Spill
	jmp	LBB4_4
LBB4_3:
	callq	___error
	movl	(%rax), %edi
	callq	_strerror
	movq	%rax, -2120(%rbp)       ## 8-byte Spill
LBB4_4:
	movq	-2120(%rbp), %rax       ## 8-byte Reload
	leaq	L_.str3(%rip), %rsi
	leaq	L_.str1(%rip), %rdx
	movl	$85, %ecx
	movq	-2112(%rbp), %rdi       ## 8-byte Reload
	movq	%rax, %r8
	movb	$0, %al
	callq	_fprintf
	movl	%eax, -2124(%rbp)       ## 4-byte Spill
	callq	___error
	movl	$0, (%rax)
	jmp	LBB4_36
LBB4_5:
	movl	$1000, %edx             ## imm = 0x3E8
	leaq	-2032(%rbp), %rsi
	leaq	-1024(%rbp), %rdi
	callq	_normal_copy
	movl	%eax, -2052(%rbp)
	cmpl	$1000, -2052(%rbp)      ## imm = 0x3E8
	je	LBB4_10
## BB#6:
	movq	___stderrp@GOTPCREL(%rip), %rax
	movq	(%rax), %rdi
	movq	%rdi, -2136(%rbp)       ## 8-byte Spill
	callq	___error
	cmpl	$0, (%rax)
	jne	LBB4_8
## BB#7:
	leaq	L_.str2(%rip), %rax
	movq	%rax, -2144(%rbp)       ## 8-byte Spill
	jmp	LBB4_9
LBB4_8:
	callq	___error
	movl	(%rax), %edi
	callq	_strerror
	movq	%rax, -2144(%rbp)       ## 8-byte Spill
LBB4_9:
	movq	-2144(%rbp), %rax       ## 8-byte Reload
	leaq	L_.str4(%rip), %rsi
	leaq	L_.str1(%rip), %rdx
	movl	$89, %ecx
	movl	-2052(%rbp), %r9d
	movq	-2136(%rbp), %rdi       ## 8-byte Reload
	movq	%rax, %r8
	movb	$0, %al
	callq	_fprintf
	movl	%eax, -2148(%rbp)       ## 4-byte Spill
	callq	___error
	movl	$0, (%rax)
	jmp	LBB4_36
LBB4_10:
	movl	$1000, %esi             ## imm = 0x3E8
	movl	$120, %edx
	leaq	-2032(%rbp), %rdi
	callq	_valid_copy
	cmpl	$0, %eax
	jne	LBB4_15
## BB#11:
	movq	___stderrp@GOTPCREL(%rip), %rax
	movq	(%rax), %rdi
	movq	%rdi, -2160(%rbp)       ## 8-byte Spill
	callq	___error
	cmpl	$0, (%rax)
	jne	LBB4_13
## BB#12:
	leaq	L_.str2(%rip), %rax
	movq	%rax, -2168(%rbp)       ## 8-byte Spill
	jmp	LBB4_14
LBB4_13:
	callq	___error
	movl	(%rax), %edi
	callq	_strerror
	movq	%rax, -2168(%rbp)       ## 8-byte Spill
LBB4_14:
	movq	-2168(%rbp), %rax       ## 8-byte Reload
	leaq	L_.str5(%rip), %rsi
	leaq	L_.str1(%rip), %rdx
	movl	$90, %ecx
	movq	-2160(%rbp), %rdi       ## 8-byte Reload
	movq	%rax, %r8
	movb	$0, %al
	callq	_fprintf
	movl	%eax, -2172(%rbp)       ## 4-byte Spill
	callq	___error
	movl	$0, (%rax)
	jmp	LBB4_36
LBB4_15:
	movl	$1000, %edx             ## imm = 0x3E8
	leaq	-2032(%rbp), %rax
	leaq	-1024(%rbp), %rdi
	movl	$121, %esi
	movl	$1000, %ecx             ## imm = 0x3E8
	movl	%ecx, %r8d
	movq	%rax, %r9
	movq	%rdi, -2184(%rbp)       ## 8-byte Spill
	movq	%r9, %rdi
	movl	%edx, -2188(%rbp)       ## 4-byte Spill
	movq	%r8, %rdx
	movq	%rax, -2200(%rbp)       ## 8-byte Spill
	callq	_memset
	movq	-2184(%rbp), %rdi       ## 8-byte Reload
	movq	-2200(%rbp), %rsi       ## 8-byte Reload
	movl	-2188(%rbp), %edx       ## 4-byte Reload
	callq	_duffs_device
	movl	%eax, -2052(%rbp)
	cmpl	$1000, -2052(%rbp)      ## imm = 0x3E8
	je	LBB4_20
## BB#16:
	movq	___stderrp@GOTPCREL(%rip), %rax
	movq	(%rax), %rdi
	movq	%rdi, -2208(%rbp)       ## 8-byte Spill
	callq	___error
	cmpl	$0, (%rax)
	jne	LBB4_18
## BB#17:
	leaq	L_.str2(%rip), %rax
	movq	%rax, -2216(%rbp)       ## 8-byte Spill
	jmp	LBB4_19
LBB4_18:
	callq	___error
	movl	(%rax), %edi
	callq	_strerror
	movq	%rax, -2216(%rbp)       ## 8-byte Spill
LBB4_19:
	movq	-2216(%rbp), %rax       ## 8-byte Reload
	leaq	L_.str6(%rip), %rsi
	leaq	L_.str1(%rip), %rdx
	movl	$97, %ecx
	movl	-2052(%rbp), %r9d
	movq	-2208(%rbp), %rdi       ## 8-byte Reload
	movq	%rax, %r8
	movb	$0, %al
	callq	_fprintf
	movl	%eax, -2220(%rbp)       ## 4-byte Spill
	callq	___error
	movl	$0, (%rax)
	jmp	LBB4_36
LBB4_20:
	movl	$1000, %esi             ## imm = 0x3E8
	movl	$120, %edx
	leaq	-2032(%rbp), %rdi
	callq	_valid_copy
	cmpl	$0, %eax
	jne	LBB4_25
## BB#21:
	movq	___stderrp@GOTPCREL(%rip), %rax
	movq	(%rax), %rdi
	movq	%rdi, -2232(%rbp)       ## 8-byte Spill
	callq	___error
	cmpl	$0, (%rax)
	jne	LBB4_23
## BB#22:
	leaq	L_.str2(%rip), %rax
	movq	%rax, -2240(%rbp)       ## 8-byte Spill
	jmp	LBB4_24
LBB4_23:
	callq	___error
	movl	(%rax), %edi
	callq	_strerror
	movq	%rax, -2240(%rbp)       ## 8-byte Spill
LBB4_24:
	movq	-2240(%rbp), %rax       ## 8-byte Reload
	leaq	L_.str7(%rip), %rsi
	leaq	L_.str1(%rip), %rdx
	movl	$98, %ecx
	movq	-2232(%rbp), %rdi       ## 8-byte Reload
	movq	%rax, %r8
	movb	$0, %al
	callq	_fprintf
	movl	%eax, -2244(%rbp)       ## 4-byte Spill
	callq	___error
	movl	$0, (%rax)
	jmp	LBB4_36
LBB4_25:
	movl	$1000, %edx             ## imm = 0x3E8
	leaq	-2032(%rbp), %rax
	leaq	-1024(%rbp), %rdi
	movl	$121, %esi
	movl	$1000, %ecx             ## imm = 0x3E8
	movl	%ecx, %r8d
	movq	%rax, %r9
	movq	%rdi, -2256(%rbp)       ## 8-byte Spill
	movq	%r9, %rdi
	movl	%edx, -2260(%rbp)       ## 4-byte Spill
	movq	%r8, %rdx
	movq	%rax, -2272(%rbp)       ## 8-byte Spill
	callq	_memset
	movq	-2256(%rbp), %rdi       ## 8-byte Reload
	movq	-2272(%rbp), %rsi       ## 8-byte Reload
	movl	-2260(%rbp), %edx       ## 4-byte Reload
	callq	_zeds_device
	movl	%eax, -2052(%rbp)
	cmpl	$1000, -2052(%rbp)      ## imm = 0x3E8
	je	LBB4_30
## BB#26:
	movq	___stderrp@GOTPCREL(%rip), %rax
	movq	(%rax), %rdi
	movq	%rdi, -2280(%rbp)       ## 8-byte Spill
	callq	___error
	cmpl	$0, (%rax)
	jne	LBB4_28
## BB#27:
	leaq	L_.str2(%rip), %rax
	movq	%rax, -2288(%rbp)       ## 8-byte Spill
	jmp	LBB4_29
LBB4_28:
	callq	___error
	movl	(%rax), %edi
	callq	_strerror
	movq	%rax, -2288(%rbp)       ## 8-byte Spill
LBB4_29:
	movq	-2288(%rbp), %rax       ## 8-byte Reload
	leaq	L_.str8(%rip), %rsi
	leaq	L_.str1(%rip), %rdx
	movl	$105, %ecx
	movl	-2052(%rbp), %r9d
	movq	-2280(%rbp), %rdi       ## 8-byte Reload
	movq	%rax, %r8
	movb	$0, %al
	callq	_fprintf
	movl	%eax, -2292(%rbp)       ## 4-byte Spill
	callq	___error
	movl	$0, (%rax)
	jmp	LBB4_36
LBB4_30:
	movl	$1000, %esi             ## imm = 0x3E8
	movl	$120, %edx
	leaq	-2032(%rbp), %rdi
	callq	_valid_copy
	cmpl	$0, %eax
	jne	LBB4_35
## BB#31:
	movq	___stderrp@GOTPCREL(%rip), %rax
	movq	(%rax), %rdi
	movq	%rdi, -2304(%rbp)       ## 8-byte Spill
	callq	___error
	cmpl	$0, (%rax)
	jne	LBB4_33
## BB#32:
	leaq	L_.str2(%rip), %rax
	movq	%rax, -2312(%rbp)       ## 8-byte Spill
	jmp	LBB4_34
LBB4_33:
	callq	___error
	movl	(%rax), %edi
	callq	_strerror
	movq	%rax, -2312(%rbp)       ## 8-byte Spill
LBB4_34:
	movq	-2312(%rbp), %rax       ## 8-byte Reload
	leaq	L_.str9(%rip), %rsi
	leaq	L_.str1(%rip), %rdx
	movl	$106, %ecx
	movq	-2304(%rbp), %rdi       ## 8-byte Reload
	movq	%rax, %r8
	movb	$0, %al
	callq	_fprintf
	movl	%eax, -2316(%rbp)       ## 4-byte Spill
	callq	___error
	movl	$0, (%rax)
	jmp	LBB4_36
LBB4_35:
	movl	$0, -2036(%rbp)
	jmp	LBB4_37
LBB4_36:
	movl	$1, -2036(%rbp)
LBB4_37:
	movq	___stack_chk_guard@GOTPCREL(%rip), %rax
	movl	-2036(%rbp), %ecx
	movq	(%rax), %rax
	cmpq	-16(%rbp), %rax
	movl	%ecx, -2320(%rbp)       ## 4-byte Spill
	jne	LBB4_39
## BB#38:                               ## %SP_return
	movl	-2320(%rbp), %eax       ## 4-byte Reload
	addq	$2312, %rsp             ## imm = 0x908
	popq	%rbx
	popq	%rbp
	retq
LBB4_39:                                ## %CallStackCheckFailBlk
	callq	___stack_chk_fail
	.cfi_endproc

	.section	__TEXT,__cstring,cstring_literals
L_.str:                                 ## @.str
	.asciz	"[ERROR] (%s:%d errno:%s) [%d] %c != %c\n"

L_.str1:                                ## @.str1
	.asciz	"ex23.c"

L_.str2:                                ## @.str2
	.asciz	"None"

L_.str3:                                ## @.str3
	.asciz	"[ERROR] (%s:%d errno:%s) Not initialized right.\n"

L_.str4:                                ## @.str4
	.asciz	"[ERROR] (%s:%d errno:%s) Normal copy failed: %d\n"

L_.str5:                                ## @.str5
	.asciz	"[ERROR] (%s:%d errno:%s) Normal copy failed.\n"

L_.str6:                                ## @.str6
	.asciz	"[ERROR] (%s:%d errno:%s) Duff's device failed: %d\n"

L_.str7:                                ## @.str7
	.asciz	"[ERROR] (%s:%d errno:%s) Duff's device failed copy.\n"

L_.str8:                                ## @.str8
	.asciz	"[ERROR] (%s:%d errno:%s) Zed's device failed: %d\n"

L_.str9:                                ## @.str9
	.asciz	"[ERROR] (%s:%d errno:%s) Zed's device failed copy.\n"


.subsections_via_symbols
