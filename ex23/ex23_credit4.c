#include<stdio.h>
#include<stdlib.h>
#include<time.h>
#include<string.h>
#include "dbg.h"
int main(int argc, char **argv)
{
	const int N = 10000;
	int SIZE = 4*1024*1024;
	void *src = malloc(SIZE);
	void *dst = malloc(SIZE);
	int i;
	
	long double memset_total = 0;
	for(i = 0; i < N; ++i)
	{
		clock_t begin = clock();
		memset(src,0,SIZE);
		memset_total += (clock() - begin);
	}
	long double memcpy_total = 0;
	for(i = 0; i < N; ++i)
	{
		clock_t	begin = clock();
		memcpy(dst,src,SIZE);
		memcpy_total += (clock() - begin);
	}

	long double memmove_total = 0;
	for(i = 0; i < N; ++i)
	{
		clock_t	begin = clock();
		memmove(dst,src,SIZE);
		memmove_total += (clock() - begin);
	}
	printf("Avg. excution time of memset function(set 4 MB blocks): %Lf ms.\n",memset_total/N);
	printf("Avg. excution time of memmove function(copy 4 MB blocks): %Lf ms.\n",memmove_total/N);
	printf("Avg. excution time of memcpy function(copy 4 MB blocks): %Lf ms.\n",memcpy_total/N);
	
	free(src);
	free(dst);
	

	SIZE=1024*1024;
	src=malloc(SIZE);
	dst=malloc(SIZE);
	memset_total = 0;
	for(i = 0; i < N; ++i)
	{
		clock_t begin = clock();
		memset(src,0,SIZE);
		memset_total += (clock() - begin);
	}
	memcpy_total = 0;
	for(i = 0; i < N; ++i)
	{
		clock_t	begin = clock();
		memcpy(dst,src,SIZE);
		memcpy_total += (clock() - begin);
	}

	 memmove_total = 0;
	for(i = 0; i < N; ++i)
	{
		clock_t	begin = clock();
		memmove(dst,src,SIZE);
		memmove_total += (clock() - begin);
	}
	printf("Avg. excution time of memset function(set 1 MB blocks): %Lf ms.\n",memset_total/N);
	printf("Avg. excution time of memmove function(copy 1 MB blocks): %Lf ms.\n",memmove_total/N);
	printf("Avg. excution time of memcpy function(copy 1 MB blocks): %Lf ms.\n",memcpy_total/N);
	free(src);
	src=NULL;
	free(dst);
	dst=NULL;
	/*
	if(memmove_total > memcpy_total)
		printf("memcpy is %Lf times faster\n", memmove_total / memcpy_total);
	else
		printf("Surprise!\nmemmove is %Lf times faster then memcpy\n", memcpy_total / memmove_total);
	*/
	
	return 0;
}
