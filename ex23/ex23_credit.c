#include <stdio.h>
#include <string.h>
#include "dbg.h"


int normal_copy(char *from, char *to, int count)
{
    int i = 0;

    for(i = 0; i < count; i++) {
        to[i] = from[i];
    }

    return i;
}
int wiki_duffs_device(register char *from, register char *to, register int count)
{
    {
        register int n = (count + 7) / 8;

        switch(count % 8) {
            case 0: do { *to = *from++;
                        case 7: *to = *from++;
                        case 6: *to = *from++;
                        case 5: *to = *from++;
                        case 4: *to = *from++;
                        case 3: *to = *from++;
                        case 2: *to = *from++;
                        case 1: *to = *from++;
                    } while(--n > 0);
        }
    }

    return count;
}

int duffs_device(char *from, char *to, int count)
{
    {
        int n = (count + 7) / 8;

        switch(count % 8) {
            case 0: do { *to++ = *from++;
                        case 7: *to++ = *from++;
                        case 6: *to++ = *from++;
                        case 5: *to++ = *from++;
                        case 4: *to++ = *from++;
                        case 3: *to++ = *from++;
                        case 2: *to++ = *from++;
                        case 1: *to++ = *from++;
                    } while(--n > 0);
        }
    }

    return count;
}

int zeds_device(char *from, char *to, int count)
{
    {
        int n = (count + 7) / 8;

        switch(count % 8) {
            case 0:
            again: *to++ = *from++;

            case 7: *to++ = *from++;
            case 6: *to++ = *from++;
            case 5: *to++ = *from++;
            case 4: *to++ = *from++;
            case 3: *to++ = *from++;
            case 2: *to++ = *from++;
            case 1: *to++ = *from++;
                    if(--n > 0) goto again;
        }
    }

    return count;
}

int valid_copy(char *data, int count, char expects)
{
    int i = 0;
    for(i = 0; i < count; i++) {
        if(data[i] != expects) {
            log_err("[%d] %c != %c", i, data[i], expects);
            return 0;
        }
    }

    return 1;
}


int main(int argc, char *argv[])
{
    char from[1000] = {'a'};
    char to[1000] = {'c'};
    register char from_r[1000] = {'a'};
    register char to_r[1000]={'c'};
    int rc = 0;

    memset(from_r,'x',1000);
    memset(to_r,'y',1000);
    // setup the from to have some stuff
    memset(from, 'x', 1000);
    // set it to a failure mode
    memset(to, 'y', 1000);
    check(valid_copy(to, 1000, 'y'), "Not initialized right.");

    // use normal copy to 
    rc = normal_copy(from, to, 1000);
    check(rc == 1000, "Normal copy failed: %d", rc);
    check(valid_copy(to, 1000, 'x'), "Normal copy failed.");

    // reset
    memset(to, 'y', 1000);

    // duffs version
    printf("Before duffs_device function...\nfrom=%s\nto=%s\n",from,to);
    rc = duffs_device(from, to, 1000);
    check(rc == 1000, "Duff's device failed: %d", rc);
    check(valid_copy(to, 1000, 'x'), "Duff's device failed copy.");
    printf("After duffs_device function...\nfrom=%s\nto=%s\n",from,to);
    // reset
    memset(to, 'y', 1000);
/*
    printf("Before [wiki]duffs_device function...\nfrom=%s\nto=%s\n",from,to);
    rc = wiki_duffs_device(from, to, 1000);
    check(rc == 1000, "Duff's device failed: %d", rc);
    //check(valid_copy(to, 1000, 'x'), "Duff's device failed copy.");
    printf("After [wiki]duffs_device function...\nfrom=%s\nto=%s\n",from,to);
	// reset
    memset(to, 'y', 1000);
*/
    printf("Before [wiki]duffs_device function...\nfrom(register)=%s\nto(register)=%s\n",from_r,to_r);
    rc = wiki_duffs_device(from_r, to_r, 1000);
    check(rc == 1000, "Duff's device failed: %d", rc);
    printf("After [wiki]duff's device function...\nfrom(register)=%s\nto(register)=%s\n",from_r,to_r);
    memset(to_r,'y', 1000);

    // my version
    printf("Before zeds_device function...\nfrom=%s\nto=%s\n",from,to);
    rc = zeds_device(from, to, 1000);
    check(rc == 1000, "Zed's device failed: %d", rc);
    check(valid_copy(to, 1000, 'x'), "Zed's device failed copy.");
    printf("After zeds_device function...\nfrom=%s\nto=%s\n",from,to);
    return 0;
error:
    return 1;
}
