#include <stdio.h>
#include <string.h>
#include "dbg.h"
#define MAX_DATA 100

typedef enum EyeColor {
	BLUE_EYES, GREEN_EYES, BROWN_EYES, 
       BLACK_EYES, OTHER_EYES
}EyeColor;

const char *EYE_COLOR_NAMES[] = {
	"Blue", "Green", "Brown", "Black", "Other"
};
typedef struct Person {
	int age;
	char first_name[MAX_DATA];
	char last_name[MAX_DATA];
	EyeColor eyes;
	float income;
}Person;
void RemoveWhiteSpaceAndNewLine(char *str)
{
	int i=0;
	for(i=strlen(str)-1;i>=0;i--)
	{
		if(str[i]!=' ' && str[i]!='\n'){
			str[i+1]='\0';
			return;
		}
	}
	return;
}
int main(int argc,char *argv[])
{
	Person you = {.age=0};
	int i=0;
	char *in=NULL;
	printf("What's your First Name? ");
	in = fgets(you.first_name,MAX_DATA-1,stdin);
	check(in!=NULL,"Failed to read first name.");
	RemoveWhiteSpaceAndNewLine(you.first_name);
	printf("what's your Last Name? ");
	in = fgets(you.last_name, MAX_DATA-1, stdin);
	check(in!=NULL,"Failed to read last name.");
	RemoveWhiteSpaceAndNewLine(you.last_name);
	printf("How old are you? ");
	int rc = fscanf(stdin,"%d",&you.age);
	check(rc>0,"You have to enter a number.");

	printf("Whar color are your eyes:\n");
	for(i=0;i<=OTHER_EYES;i++) {
		printf("%d) %s\n",i+1,EYE_COLOR_NAMES[i]);
	}
	printf("> ");

	int eyes = -1;
	rc = fscanf(stdin, "%d", &eyes);
	check(rc>0, "You have to enter a number.");
	you.eyes = eyes-1;
	check(you.eyes <= OTHER_EYES && you.eyes >=0,"Do it right, that's not an option.");
	printf("How much do you make an hour? ");
	rc = fscanf(stdin, "%f", &you.income);
	check(rc>0, "Enter a floating point number.");

	printf("---- RESULTS ----\n");
	printf("First Name(length=%lu): %s",strlen(you.first_name),you.first_name);
	printf("Last Name(length=%lu): %s",strlen(you.last_name),you.last_name);
	printf("Age: %d\n", you.age);
	printf("Eyes: %s\n",EYE_COLOR_NAMES[you.eyes]);
	printf("Income: %f\n",you.income);
	return 0;
error:
	return -1;
}
