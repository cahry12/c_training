#include <stdio.h>
#include <string.h>
#include "dbg.h"
#define MAX_DATA 100
int fun(char *str){
	int len=0,rc;
	char ch;
	do{
		rc=scanf("%c",&ch);
		if(ch=='\n')break;
		//check(rc>0,"Failed to read character");
		//printf("ch=%d,%c\n",ch,ch);
		str[len++]=ch;
	}while( ch!='\n' && rc!=EOF );
	str[len++]='\0';
	return strlen(str);
error:
	return -1;
}
int main(int argc,char *argv[])
{
	char name[MAX_DATA];
	int name_idx=0;
	int i=0;
	int size=fun(name);
	if(size>0)printf("name(length=%d)=%s\n",size,name);
	return 0;
error:
	return -1;
}
