#include "minunit.h"
#include <lcthw/ringbuffer.h>
#include <assert.h>
#include <string.h>
#include <time.h>
char *tests[]={"ex44testing...","HelloQueue","ringbuffer"};
#define RING_LEN 50
#define READ_TEST 4
#define WRITE_CASE 3
#define BUFFER_SIZE 20

static RingBuffer *ringbuffer=NULL;

char *test_create(){
	clock_t begin,end;
	FILE *file=fopen("tests/ringbuffer_create_pf","a");
	begin=clock();
	ringbuffer = RingBuffer_create(RING_LEN);
	end=clock();
	fprintf(file,"%lf\n",(double)(end-begin)/CLOCKS_PER_SEC);
	fclose(file);

	mu_assert(ringbuffer!=NULL,"Failed to create ring buffer.");
	return NULL;
}
char *test_read_write(){
	int rec;
	FILE *file;
	file=fopen("tests/ringbuffer_write_pf","a");
	clock_t begin,end;
	double avg_processing_time=0.0;
	begin=clock();
	for(int idx=0;idx<WRITE_CASE;idx++){
		rec=RingBuffer_write(ringbuffer,tests[idx],strlen(tests[idx]));
		mu_assert(rec==(int)strlen(tests[idx]),"Wrong count on writing");
	}
	end=clock();
	avg_processing_time+=(double)(end-begin)/CLOCKS_PER_SEC;
	fprintf(file,"%lf\n",avg_processing_time/WRITE_CASE);
	fclose(file);
	char *buffer=calloc(BUFFER_SIZE,sizeof(char *));
	mu_assert(buffer!=NULL,"Failed to create buffer through calloc function.");
	// char *buffer=NULL; //ERROR! Due to reason that buffer wouldn't allocate memory
	avg_processing_time=0.0;
	file=fopen("tests/ringbuffer_read_pf","a");
	begin=clock();
	rec=RingBuffer_read(ringbuffer,buffer,READ_TEST);
	printf("Reading content from ringbuffer with size %d:%s(copy to buffer)\n",READ_TEST,buffer);
	mu_assert(rec==READ_TEST,"Wrong count on reading");
	rec=RingBuffer_read(ringbuffer,buffer,READ_TEST+2);
	printf("Reading content from ringbuffer with size %d:%s(copy to buffer)\n",READ_TEST+2,buffer);
	mu_assert(rec==READ_TEST+2,"Wrong count on reading");
	rec=RingBuffer_read(ringbuffer,buffer,READ_TEST+6);
	printf("Reading content from ringbuffer with size %d:%s(copy to buffer)\n",READ_TEST+6,buffer);
	mu_assert(rec==READ_TEST+6,"Wrong count on reading");
	end=clock();
	avg_processing_time+=(double)(end-begin)/CLOCKS_PER_SEC;
	fprintf(file,"%lf\n",avg_processing_time/3);
	fclose(file);
	/*
	rec=RingBuffer_read(ringbuffer,buffer,READ_TEST+15);
	printf("Reading content from ringbuffer with size %d:%s(copy to buffer)\n",READ_TEST+15,buffer);
	*/
	//more than the size of available data, thus RingBuffer_read return -1 to rec 
	//mu_assert(rec==READ_TEST+15,"Wrong count on reading");
	return NULL;
}
char *test_destroy(){
	mu_assert(ringbuffer!=NULL,"Failed to make ring buffer #2");
	clock_t begin,end;
	FILE *file=fopen("tests/ringbuffer_destroy_pf","a");
	begin=clock();
	RingBuffer_destroy(ringbuffer);
	end=clock();
	fprintf(file,"%lf\n",(double)(end-begin)/CLOCKS_PER_SEC);
	fclose(file);
	return NULL;
}
char *all_tests(){
	mu_suite_start();
	mu_run_test(test_create);
	mu_run_test(test_read_write);
	mu_run_test(test_destroy);	
	return NULL;
}
RUN_TESTS(all_tests);
