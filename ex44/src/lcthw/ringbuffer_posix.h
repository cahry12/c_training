#ifndef _lcthw_PRingBuffer_POSIX_h
#define _lcthw_PRingBuffer_POSIX_h
#include <lcthw/bstrlib.h>
#define report_exceptional_condition() abort ()
typedef struct {
    void *address;
    int count_bytes;
    int write_offset_bytes;
    int read_offset_bytes;
} PRingBuffer;

PRingBuffer *PRingBuffer_create(int length);

void PRingBuffer_destroy(PRingBuffer *buffer);

int PRingBuffer_read(PRingBuffer *buffer, char *target, int amount);

int PRingBuffer_write(PRingBuffer *buffer, char *data, int length);

int PRingBuffer_empty(PRingBuffer *buffer);

int PRingBuffer_full(PRingBuffer *buffer);

int PRingBuffer_available_data(PRingBuffer *buffer);

int PRingBuffer_available_space(PRingBuffer *buffer);

bstring PRingBuffer_gets(PRingBuffer *buffer, int amount);

#define PRingBuffer_available_data(B) ((B)->write_offset_bytes - (B)->read_offset_bytes)

#define PRingBuffer_available_space(B) ((B)->count_bytes - PRingBuffer_available_data(B))

#define PRingBuffer_puts(B, D) PRingBuffer_write((B), bdata((D)), blength((D)))

#define PRingBuffer_get_all(B) PRingBuffer_gets((B), PRingBuffer_available_data((B)))

#define PRingBuffer_starts_at(B) ((B)->address + (B)->read_offset_bytes)

#define PRingBuffer_ends_at(B) ((B)->address + (B)->write_offset_bytes)

#define PRingBuffer_commit_read(B, A) ((B)->read_offset_bytes = ((B)->read_offset_bytes + (A)) % (B)->count_bytes)

#define PRingBuffer_commit_write(B, A) ((B)->write_offset_bytes = ((B)->write_offset_bytes + (A)) % (B)->count_bytes)

#endif
