#include <sys/mman.h>
#include <stdlib.h>
#include <unistd.h>

#define report_exceptional_condition() abort ()

struct ring_buffer
{
  void *address;

  unsigned long count_bytes;
  unsigned long write_offset_bytes;
  unsigned long read_offset_bytes;
};

//Warning order should be at least 12 for Linux
void
ring_buffer_create (struct ring_buffer *buffer, unsigned long order)
{
  char path[] = "/dev/shm/ring-buffer-XXXXXX";
  int file_descriptor;
  void *address;
  int status;

  file_descriptor = mkstemp (path);  // mkstemp用來建立臨時檔案文件,名稱最後六個字元必须是XXXXXX,如果檔案不存在則建立;
  if (file_descriptor < 0)
    report_exceptional_condition (); //中斷

  status = unlink (path); //unlink會刪除path指定檔案文件
  if (status)
    report_exceptional_condition ();

  buffer->count_bytes = 1UL << order;  //1UL== unsigned long的1
  buffer->write_offset_bytes = 0;
  buffer->read_offset_bytes = 0;

  status = ftruncate (file_descriptor, buffer->count_bytes); //將參數file_descriptor 指定的文件大小改為參數buffer->count_bytes 指定的大小。成功返回0失敗則回傳-1
  if (status)
    report_exceptional_condition ();

  buffer->address = mmap (NULL, buffer->count_bytes << 1, PROT_NONE, MAP_ANON | MAP_PRIVATE, -1, 0);//記憶體映射函數 mmap，它把文件內容映射到一段記憶體上。
  // 函數：void *mmap(void *start,size_t length, int prot, int flags, int fd, off_t offsize); \
  // 參數start：指向欲映射的核心起始位址，通常設為NULL，代表讓系統自動選定位址，核心會自己在進程位址空間中選擇合適的位址建立\
  // 參數length：代表映射的大小。將文件的多大長度映射到記憶體。 \\
  // 參數prot：映射區域的保護方式。可以為以下幾種方式的組合：PROT_NONE 映射區域不能存取 \\
  // 參數flags：影響映射區域的各種特性。在調用mmap()時必須要指定MAP_SHARED 或MAP_PRIVATE。\\
  // MAP_PRIVATE 不允許其他映射該文件的行程共享，對映射區域的寫入操作會產生一個映射的複製(copy-on-write)，對此區域所做的修改不會寫回原文件。 
  // MAP_ANONYMOUS(OSX version: MAP_ANON) 建立匿名映射。此時會忽略參數fd，不涉及文件，而且映射區域無法和其他進程共享。 \\
  // 參數fd：由open返回的文件描述符，代表要映射到核心中的文件。如果使用匿名核心映射時，即flags中設置了MAP_ANONYMOUS，fd設為-1。\\
  // 參數offset：從文件映射開始處的偏移量，通常為0，代表從文件最前方開始映射。

  if (buffer->address == MAP_FAILED) //若映射成功則返回映射區的核心起始位址，否則返回MAP_FAILED(-1)，錯誤原因存於errno 中。
    report_exceptional_condition ();

  address =
    mmap (buffer->address, buffer->count_bytes, PROT_READ | PROT_WRITE,
          MAP_FIXED | MAP_SHARED, file_descriptor, 0);

  if (address != buffer->address)
    report_exceptional_condition ();

  address = mmap (buffer->address + buffer->count_bytes,
                  buffer->count_bytes, PROT_READ | PROT_WRITE,
                  MAP_FIXED | MAP_SHARED, file_descriptor, 0);

  if (address != buffer->address + buffer->count_bytes)
    report_exceptional_condition ();

  status = close (file_descriptor);
  if (status)
    report_exceptional_condition ();
}

void
ring_buffer_free (struct ring_buffer *buffer)
{
  int status;

  status = munmap (buffer->address, buffer->count_bytes << 1);  //解除記憶體映射
  if (status)
    report_exceptional_condition ();
}

void *
ring_buffer_write_address (struct ring_buffer *buffer)
{
  /*** void pointer arithmetic is a constraint violation. ***/
  return buffer->address + buffer->write_offset_bytes;
}

void
ring_buffer_write_advance (struct ring_buffer *buffer,
                           unsigned long count_bytes)
{
  buffer->write_offset_bytes += count_bytes;
}

void *
ring_buffer_read_address (struct ring_buffer *buffer)
{
  return buffer->address + buffer->read_offset_bytes;
}

void
ring_buffer_read_advance (struct ring_buffer *buffer,
                          unsigned long count_bytes)
{
  buffer->read_offset_bytes += count_bytes;

  if (buffer->read_offset_bytes >= buffer->count_bytes)
    {
      buffer->read_offset_bytes -= buffer->count_bytes;
      buffer->write_offset_bytes -= buffer->count_bytes;
    }
}

unsigned long
ring_buffer_count_bytes (struct ring_buffer *buffer)
{
  return buffer->write_offset_bytes - buffer->read_offset_bytes;
}

unsigned long
ring_buffer_count_free_bytes (struct ring_buffer *buffer)
{
  return buffer->count_bytes - ring_buffer_count_bytes (buffer);
}

void
ring_buffer_clear (struct ring_buffer *buffer)
{
  buffer->write_offset_bytes = 0;
  buffer->read_offset_bytes = 0;
}
