#undef NDEBUG
#include <assert.h>
#include <stdio.h>
#include <string.h>
#include <lcthw/dbg.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <stdlib.h>
#include <unistd.h>
#include <lcthw/ringbuffer_posix.h>
#define FILE_NAME "shmem_f"
int file_descriptor;
PRingBuffer *PRingBuffer_create(int length)
{
	
    void *address=NULL;
    int status;
    file_descriptor = shm_open(FILE_NAME, O_CREAT | O_RDWR, 0777);
    if (file_descriptor <0)
	    report_exceptional_condition();
    PRingBuffer *buffer = calloc(1, sizeof(PRingBuffer));
    buffer->count_bytes  = 1UL << length;
    buffer->read_offset_bytes = 0;
    buffer->write_offset_bytes = 0;

    status = ftruncate (file_descriptor, buffer->count_bytes);
    if (status)
      report_exceptional_condition ();
    buffer->address = mmap (NULL, buffer->count_bytes << 1, PROT_NONE, MAP_ANON | MAP_PRIVATE, -1, 0);
	
    if (buffer->address == MAP_FAILED)
	report_exceptional_condition ();

    //======================Address TEST[START]======================
    address =
    mmap (buffer->address, buffer->count_bytes, PROT_READ | PROT_WRITE,
          MAP_FIXED | MAP_SHARED, file_descriptor, 0);

    if (address != buffer->address)
      report_exceptional_condition ();
    address = mmap (buffer->address+buffer->count_bytes,buffer->count_bytes, PROT_READ | PROT_WRITE, MAP_FIXED | MAP_SHARED, file_descriptor, 0);
    /*
    if (address != buffer->address + buffer->count_bytes)
      report_exceptional_condition ();
*/
 //   printf("==========測試7=============,address=%p,buffer->address=%p\n",address,buffer->address+buffer->count_bytes);
 //   ======================Address TEST[END]======================
    status = close (file_descriptor);
    if (status)
      report_exceptional_condition ();
    return buffer;
}

void PRingBuffer_destroy(PRingBuffer *buffer)
{
    if(buffer) {
	int status;
	status = munmap (buffer->address, buffer->count_bytes << 1);  //解除記憶體映射
  	if (status)
    	  report_exceptional_condition ();
        free(buffer);
	buffer=NULL;
	shm_unlink(FILE_NAME);
    }
}

int PRingBuffer_write(PRingBuffer *buffer, char *data, int length)
{
    if(PRingBuffer_available_data(buffer) == 0) {
        buffer->read_offset_bytes = buffer->write_offset_bytes = 0;
    }
    check(length <= PRingBuffer_available_space(buffer),
            "Not enough space: %d request, %d available",
            PRingBuffer_available_data(buffer), length);
    void *address=buffer->address+buffer->write_offset_bytes;
    //void *address = mmap(buffer->address+buffer->write_offset_bytes,length,PROT_READ | PROT_WRITE,
//		    MAP_FIXED | MAP_SHARED, file_descriptor, 0);
    strcpy(address,data);
    check(address != NULL, "Failed to write buffer into data.");
    //printf("address's contents=%s\n",buffer->address);
	
    PRingBuffer_commit_write(buffer, length);

    return length;
error:
    return -1;
}

int PRingBuffer_read(PRingBuffer *buffer, char *target, int amount)
{
    check_debug(amount <= PRingBuffer_available_data(buffer),
            "Not enough in the buffer: has %d, needs %d",
            PRingBuffer_available_data(buffer), amount);
/*
    target = mmap (buffer->address, amount, PROT_READ | PROT_WRITE,
		              MAP_FIXED | MAP_SHARED, file_descriptor, 0);
    target = target+buffer->read_offset_bytes;
*/
    memcpy(target, buffer->address+buffer->read_offset_bytes,amount);
    //target = mmap(buffer->address+buffer->read_offset_bytes,amount,PROT_READ | PROT_WRITE,
//		    MAP_FIXED | MAP_SHARED, file_descriptor, 0);
    check(target != NULL, "Failed to write buffer into data.");

    PRingBuffer_commit_read(buffer, amount);
	
    if (buffer->read_offset_bytes >= buffer->count_bytes)
        {
	      buffer->read_offset_bytes -= buffer->count_bytes;
              buffer->write_offset_bytes -= buffer->count_bytes;
        }
    return amount;
error:
    return -1;
}

bstring PRingBuffer_gets(PRingBuffer *buffer, int amount)
{
    check(amount > 0, "Need more than 0 for gets, you gave: %d ", amount);
    check_debug(amount <= PRingBuffer_available_data(buffer),
            "Not enough in the buffer.");

    bstring result = blk2bstr(PRingBuffer_starts_at(buffer), amount);
    check(result != NULL, "Failed to create gets result.");
    check((int)blength(result) == amount, "Wrong result length.");

    PRingBuffer_commit_read(buffer, amount);
    assert(PRingBuffer_available_data(buffer) >= 0 && "Error in read commit.");

    return result;
error:
    return NULL;
}
