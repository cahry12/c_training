#include "minunit.h"
#include <lcthw/darray.h>
#include <lcthw/darray_algos.h>
#include <time.h>
#include <stdint.h>
#include <string.h>
#define FUNCTION_CALL_SIZE 8
#define TEST_SIZE 10
#define STR_LEN 10
#define NEW_NUM_VALUES 5000
static DArray *array = NULL;
#ifndef DEBUG
//#define DEBUG
int global_idx=0;
#endif
//static int *val1 = NULL;
//static int *val2 = NULL;
int **val = NULL;
char *values[] = {"XXXX", "1234", "abcd", "xjvef", "NDSS"};
int is_sorted(DArray *array)
{
    int i ;
    for(i = 0; i < DArray_count(array) - 1; i++) {
        if(strcmp(DArray_get(array, i), DArray_get(array, i+1)) > 0) {
            return 0;
        }
    }

    return 1;
}
void random_gen_str(char *s, const int len) {
    const char alpha_and_num[] =
        "0123456789"
        "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
        "abcdefghijklmnopqrstuvwxyz";

    for (int i = 0; i < len; ++i) {
        s[i] = alpha_and_num[rand() % (sizeof(alpha_and_num) - 1)];
    }
    s[len] = '\0';
#ifdef DEBUG
    printf("[GENERATING STRING AT INDEX %d,len=%d]=>",++global_idx,len);
    puts(s);
#endif
}
DArray *create_words_by_gen(int size)
{
        DArray *words = DArray_create(0,size);
        char *auto_str=NULL;
        int i,len;
        for(i=0;i<size;i++){
                len = (rand() % STR_LEN)+1;
                if(len==1)len++;
                auto_str = malloc(sizeof(len));
                random_gen_str(auto_str,len-1);
                DArray_push(words,auto_str);
        }
        return words;
}
DArray *create_words()
{
    DArray *result = DArray_create(0, 5);
    char *words[] = {"asdfasfd", "werwar", "13234", "asdfasfd", "oioj"};
    int i = 0;

    for(i = 0; i < 5; i++) {
        DArray_push(result, words[i]);
    }

    return result;
}
char *test_create()
{
    array = DArray_create(sizeof(int), 100);
    mu_assert(array != NULL, "DArray_create failed.");
    mu_assert(array->contents != NULL, "contents are wrong in darray");
    mu_assert(array->end == 0, "end isn't at the right spot");
    mu_assert(array->element_size == sizeof(int), "element size is wrong.");
    mu_assert(array->max == 100, "wrong max length on initial size");
    
    val = calloc(TEST_SIZE, sizeof(int *));
    return NULL;
}

char *test_destroy()
{
    DArray_destroy(array);

    return NULL;
}

char *test_new()
{
    for(int idx=0;idx<TEST_SIZE;idx++){
	    //printf("[function:test_new]idx=%d\n",idx);
	    *(val+idx)=DArray_new(array);
    	    mu_assert(*(val+idx) != NULL,"failed to make a new element");
    }
    /*
    val1 = DArray_new(array);
    mu_assert(val1 != NULL, "failed to make a new element");

    val2 = DArray_new(array);
    mu_assert(val2 != NULL, "failed to make a new element");
*/
    return NULL;
}

char *test_set()
{
	
    for(int idx=0;idx<TEST_SIZE;idx++)
	                DArray_set(array,idx,*(val+idx));
    return NULL;
}

char *test_get()
{
    for(int idx=0;idx<TEST_SIZE;idx++)
	    mu_assert(DArray_get(array,idx) == *(val+idx), "Wrong value");
    //mu_assert(DArray_get(array, 0) == val1, "Wrong first value.");
    //mu_assert(DArray_get(array, 1) == val2, "Wrong second value.");

    return NULL;
}

char *test_remove()
{
    int *val_check = NULL;
    for(int idx=0;idx<TEST_SIZE;idx++){
    	val_check = DArray_remove(array,idx);
	mu_assert(val_check != NULL, "Should not get NULL.");
	//mu_assert(*val_check == *(*(val+idx)),"Should get the value.");
	mu_assert(DArray_get(array,idx) == NULL, "Should be gone.");
	DArray_free(val_check);
    }
    /*
    int *val_check = DArray_remove(array, 0);
    mu_assert(val_check != NULL, "Should not get NULL.");
    mu_assert(*val_check == *val1, "Should get the first value.");
    mu_assert(DArray_get(array, 0) == NULL, "Should be gone.");
    DArray_free(val_check);

    val_check = DArray_remove(array, 1);
    mu_assert(val_check != NULL, "Should not get NULL.");
    mu_assert(*val_check == *val2, "Should get the first value.");
    mu_assert(DArray_get(array, 1) == NULL, "Should be gone.");
    DArray_free(val_check);*/

    return NULL;
}

char *test_expand_contract()
{
    int old_max = array->max;
    DArray_expand(array);
    mu_assert((unsigned int)array->max == old_max + array->expand_rate, "Wrong size after expand.");

    DArray_contract(array);
    mu_assert((unsigned int)array->max == array->expand_rate + 1, "Should stay at the expand_rate at least.");

    DArray_contract(array);
    mu_assert((unsigned int)array->max == array->expand_rate + 1, "Should stay at the expand_rate at least.");

    return NULL;
}

char *test_push_pop()
{
    int i = 0;
    for(i = 0; i < 1000; i++) {
        int *val = DArray_new(array);
        *val = i * 333;
        DArray_push(array, val);
    }

    mu_assert(array->max == 1201, "Wrong max size.");

    for(i = 999; i >= 0; i--) {
        int *val = DArray_pop(array);
        mu_assert(val != NULL, "Shouldn't get a NULL.");
        mu_assert(*val == i * 333, "Wrong value.");
        DArray_free(val);
    }

    return NULL;
}
char *test_bubblesort()
{
	DArray *words = create_words();
	mu_assert(!is_sorted(words),"Words should start not sorted.");
	int rc = DArray_bubble_sort(words,(DArray_compare)strcmp);
	mu_assert(rc==0,"Bubble sort failed");
	mu_assert(is_sorted(words),"Words aren't sorted after bubble sort.");
	//DArray_destroy(words);
	return NULL;
}
char *test_mergesort()
{
	DArray *words = create_words();
	mu_assert(!is_sorted(words),"Words should start not sorted.");
	int rc = DArray_merge_sort(words, 0, DArray_count(words)-1, (DArray_compare)strcmp);
	mu_assert(rc==0,"Merge sort failed");
	mu_assert(is_sorted(words),"Words aren't sorted after merge sort.");	
	//DArray_destroy(words);
	return NULL;
}
char *test_different_size_merge_and_bubble_performance()
{
	clock_t begin,end;
        double avg_processing_time=0.0;
        FILE *merge_file=fopen("tests/merge_data_darray_ds","a");
        FILE *bubble_file=fopen("tests/bubble_data_darray_ds","a");
        mu_assert(merge_file,"Create merge file failed!");
        mu_assert(bubble_file,"Create bubble file failed!");
        int run_times=10;
	int size=10;
	DArray *words=NULL;
	int i=0,rc;
	while(size<NEW_NUM_VALUES){
		i=0,avg_processing_time=0.0;
		while(i++<run_times){
			words = create_words_by_gen(size);
			begin = clock();
			rc = DArray_bubble_sort(words,(DArray_compare)strcmp);
			end = clock();	
			mu_assert(rc==0,"Bubble sort failed");
			mu_assert(is_sorted(words),"Words aren't sorted after bubble sort.");
			avg_processing_time+=(double)(end-begin)/ CLOCKS_PER_SEC;
			DArray_destroy(words);
			words=NULL;
		}
		fprintf(bubble_file,"%d %.10f\n",size,avg_processing_time/(double)run_times);
		i=0;avg_processing_time=0.0;
		while(i++<run_times){
			words = create_words_by_gen(size);
			begin = clock();
			rc = DArray_merge_sort(words,0, DArray_count(words)-1, (DArray_compare)strcmp);
			end = clock();
			mu_assert(rc==0,"Merge sort failed");
			mu_assert(is_sorted(words),"Words aren't sorted after merge sort.");
			avg_processing_time+=(double)(end-begin)/ CLOCKS_PER_SEC;
			DArray_destroy(words);
			words=NULL;
		}
		fprintf(merge_file,"%d %.10f\n",size,avg_processing_time/(double)run_times);
		
		size+=100;
	}
	fclose(merge_file);
	fclose(bubble_file);
	return NULL;
}
char (*fun)();
char * all_tests() {
    mu_suite_start();
    
    //mu_run_test(test_bubblesort);
    //mu_run_test(test_mergesort);
    mu_run_test(test_different_size_merge_and_bubble_performance);
   return NULL;
}

RUN_TESTS(all_tests);
