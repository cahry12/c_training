#include "minunit.h"
#include <lcthw/darray.h>
#include <lcthw/list.h>
#include <time.h>
#include <string.h>
#define FUNCTION_CALL_SIZE 9
static DArray *array = NULL;
static List *list = NULL;
static int *val1 = NULL;
static int *val2 = NULL;

char *test_create()
{
    // A part of array[begin]
    array = DArray_create(sizeof(int), 100);
    mu_assert(array != NULL, "DArray_create failed.");
    mu_assert(array->contents != NULL, "contents are wrong in darray");
    mu_assert(array->end == 0, "end isn't at the right spot");
    mu_assert(array->element_size == sizeof(int), "element size is wrong.");
    mu_assert(array->max == 100, "wrong max length on initial size");
    // A part of array[end]
    //
    // A part of link list[begin]
    list = List_create();
    mu_assert(list != NULL, "Failed to create list.");
// A part of link list[end]
    return NULL;
}

char *test_destroy()
{
    DArray_destroy(array);

    List_destroy(list);
    return NULL;
}

char *test_new()
{
    val1 = DArray_new(array);
    mu_assert(val1 != NULL, "failed to make a new element");

    val2 = DArray_new(array);
    mu_assert(val2 != NULL, "failed to make a new element");

    return NULL;
}

char *test_set()
{
    DArray_set(array, 0, val1);
    DArray_set(array, 1, val2);

    return NULL;
}

char *test_get()
{
    mu_assert(DArray_get(array, 0) == val1, "Wrong first value.");
    mu_assert(DArray_get(array, 1) == val2, "Wrong second value.");

    return NULL;
}

char *test_remove()
{
    double avg_processing_time=0.0;
    clock_t begin,end;
    FILE *remove_file = fopen("tests/darray_remove_file","a");
  
    mu_assert(remove_file,"Open remove file failed!");
  
    begin=clock();
    int *val_check = DArray_remove(array, 0);
    end=clock();
    avg_processing_time=(double)(end-begin)/CLOCKS_PER_SEC;
    fprintf(remove_file,"%.10f\n",avg_processing_time);
    fclose(remove_file);
  
    mu_assert(val_check != NULL, "Should not get NULL.");
    mu_assert(*val_check == *val1, "Should get the first value.");
    mu_assert(DArray_get(array, 0) == NULL, "Should be gone.");
    DArray_free(val_check);

    val_check = DArray_remove(array, 1);
    mu_assert(val_check != NULL, "Should not get NULL.");
    mu_assert(*val_check == *val2, "Should get the first value.");
    mu_assert(DArray_get(array, 1) == NULL, "Should be gone.");
    DArray_free(val_check);
    
    return NULL;
}
char *test_list_remove()
{
    clock_t begin,end;
    double avg_processing_list_time=0.0;
    FILE *remove_list_file = fopen("tests/list_remove_file","a");
    mu_assert(remove_list_file,"Open remove file failed(link list)!");
    int i=1;
    int *val = DArray_new(array);
    *val = i * 333;
    List_push(list, val);

    begin = clock();
    int *val_list_check = List_remove(list, list->first->next);
    end = clock();
    //mu_assert(val_list_check != NULL, "Should not get NULL.");
    avg_processing_list_time=(double)(end-begin)/CLOCKS_PER_SEC;
    fprintf(remove_list_file,"%.10f\n",avg_processing_list_time);
    fclose(remove_list_file);
    DArray_free(val_list_check);
    return NULL;
}
char *test_expand_contract()
{
    int old_max = array->max;
    DArray_expand(array);
    mu_assert((unsigned int)array->max == old_max + array->expand_rate, "Wrong size after expand.");

    DArray_contract(array);
    mu_assert((unsigned int)array->max == array->expand_rate + 1, "Should stay at the expand_rate at least.");

    DArray_contract(array);
    mu_assert((unsigned int)array->max == array->expand_rate + 1, "Should stay at the expand_rate at least.");

    return NULL;
}

char *test_push_pop()
{
    clock_t begin, end;
    FILE *push_file = fopen("tests/darray_push_file","a"), *push_list_file = fopen("tests/list_push_file","a");
    mu_assert(push_file,"Open push file failed!");
    mu_assert(push_list_file,"Open push file failed!");
    double avg_processing_time=0.0,avg_processing_list_time=0.0;
    int i = 0;
    for(i = 0; i < 1000; i++) {
        int *val = DArray_new(array);
        *val = i * 333;
	begin = clock();
        DArray_push(array, val);
	end = clock();
	avg_processing_time+=(double)(end-begin)/ CLOCKS_PER_SEC;

	begin = clock();
	List_push(list, val);
	end = clock();
	mu_assert(List_last(list) == val, "Wrong value on pop.");
	avg_processing_list_time+=(double)(end-begin)/ CLOCKS_PER_SEC;
    }
    fprintf(push_file,"%.10f\n",avg_processing_time/(double)1000.0);
    fprintf(push_list_file,"%.10f\n",avg_processing_list_time/(double)1000.0);
    fclose(push_file);
    fclose(push_list_file);
    mu_assert(array->max == 1201, "Wrong max size.");

    avg_processing_time=0.0,avg_processing_list_time=0.0;
    FILE *pop_file = fopen("tests/darray_pop_file","a"),*pop_list_file = fopen("tests/list_pop_file","a");;
    mu_assert(pop_file,"Open pop file failed!");
    mu_assert(pop_list_file,"Open pop file failed!(link list)");
    for(i = 999; i >= 0; i--) {
	begin = clock();
        int *val = DArray_pop(array);
        end = clock();
	avg_processing_time+=(double)(end-begin)/ CLOCKS_PER_SEC;
	mu_assert(val != NULL, "Shouldn't get a NULL.");
        mu_assert(*val == i * 333, "Wrong value.");
        
	begin = clock();
	val = List_pop(list);
	end = clock();
	avg_processing_list_time+=(double)(end-begin)/ CLOCKS_PER_SEC;
	mu_assert(val != NULL, "Shouldn't get a NULL.");
	DArray_free(val);

    }
    fprintf(pop_file,"%.10f\n",avg_processing_time/(double)1000.0);
    fprintf(pop_list_file,"%.10f\n",avg_processing_list_time/(double)1000.0);
    fclose(pop_file);
    fclose(pop_list_file);
    return NULL;
}
char (*fun)();
static char* (*function[FUNCTION_CALL_SIZE])() = {test_create, test_new, test_set, test_get, test_remove, test_expand_contract, test_push_pop, test_list_remove, test_destroy};  // Array of function
char * all_tests() {
    mu_suite_start();
/*
    mu_run_test(test_create);
mu_run_tes:Q!
t(test_new);
    mu_run_test(test_set);
    mu_run_test(test_get);
    mu_run_test(test_remove);
    mu_run_test(test_expand_contract);
    mu_run_test(test_push_pop);
    mu_run_test(test_destroy);
*/
    
    for(int i=0;i<FUNCTION_CALL_SIZE;i++){
 	    char* (*fptr)()=function[i];
	    mu_run_test(fptr);
    }
   return NULL;
}

RUN_TESTS(all_tests);
