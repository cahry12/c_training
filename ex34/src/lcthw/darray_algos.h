#ifndef lcthw_darray_algos_h
#define lcthw_darray_algos_h
#include<lcthw/darray.h>
typedef int (*DArray_compare)(const void *a, const void *b);
int DArray_bubble_sort(DArray *array, DArray_compare cmp);
int DArray_merge_sort(DArray *array,int left,int right, DArray_compare cmp);
#endif
