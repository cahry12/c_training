#ifndef lcthw_list_algos_h
#define lcthw_list_algos_h
#include <lcthw/list.h>

typedef int (*List_compare)(const void *a, const void *b);
int List_bubble_sort(List *list, List_compare cmp);

List * List_merge_sort(List *list, List_compare cmp);
List * List_merge_bottomup_sort(List *list, List_compare cmp);

List* List_insert_sorted(List *list, void * value, List_compare cmp);
#define COUNT_FOREACH(len) int i=0;\
    for(i=0; i<len-1; i++)
#define WIDTH_DOREACH(total_len) int width;\
   for(width=1; width<total_len;width=2*width)
#define MIN(x,y) (((x)<(y))?(x):(y))
#endif
