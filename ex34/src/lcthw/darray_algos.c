#include <lcthw/darray_algos.h>
#include <lcthw/dbg.h>
#include <assert.h>
#include <stdint.h>

int DArray_bubble_sort(DArray *array, DArray_compare cmp)
{
	check(array != NULL,"array can't be NULL!");
	if(DArray_count(array)<=1)
		return 0;
	for(int idx=0;idx<DArray_count(array)-1;idx++){
		for(int intra_idx=0;intra_idx<DArray_count(array)-idx-1;intra_idx++)
			if(cmp(array->contents[intra_idx],array->contents[intra_idx+1])>0){
					void *temp=array->contents[intra_idx];
					array->contents[intra_idx]=array->contents[intra_idx+1];
					array->contents[intra_idx+1]=temp;
					}
	}
	return 0;
error:
	return -1;
}
int DArray_merge_sort(DArray *array, int left, int right, DArray_compare cmp)
{
	check(array != NULL,"array can't be NULL!");
	if(left < right){
		//printf("C1,left=%d,right=%d,",left,right);
		//void **temp;
		//printf("temp=%p\n",temp);
		int middle = (left+right)/2;
		DArray_merge_sort(array,left,middle,cmp);
		DArray_merge_sort(array,middle+1,right,cmp);
		void **temp;
		temp=calloc(right,sizeof(void *));
		
		/*for(int idx=0,l_idx=left,r_idx=middle+1;idx<DArray_count(array);idx++){
			if( (l_idx!=middle && cmp(array->contents[l_idx],array->contents[r_idx])<=0)
					|| r_idx==right){
				temp[idx]=array->contents[l_idx++];
				//do_nothing
				//DArray_push(array_sorted,array->contents[l_idx++]);
			}
			else {
				temp[idx]=array->contents[r_idx++];
				//array->contents[l_idx++]=array->contents[r_idx];
				//array->contents[r_idx]=temp;
				//DArray_push(array_sorted,new_right->contents[r_idx++]);
			}
		}*/
		int l_idx=left,r_idx=middle+1,idx=0;
		while(l_idx<=middle && r_idx<=right) {
			if(cmp(array->contents[l_idx],array->contents[r_idx])<=0){
				temp[idx++]=array->contents[l_idx++];
			}else {
				temp[idx++]=array->contents[r_idx++];
			}
		}
		while(l_idx<=middle){
			temp[idx++]=array->contents[l_idx++];
		}
		while(r_idx<=right){
			temp[idx++]=array->contents[r_idx++];
		}
		for(int idx=left,idx2=0;idx<=right;idx++,idx2++){
			//void *free_mem=&array->contents[idx];
			array->contents[idx]=temp[idx2];
		}
		//printf("C2,left=%d,right=%d,",left,right);
		//printf("temp=%p\n",temp);
		//free(temp);
		//printf("C3,left=%d,right=%d\n",left,right);
	}
	/*
	DArray *array_sorted = DArray_create(sizeof(int), 100);
	for(int idx=0,l_idx=0,r_idx=0;idx<DArray_count(array);idx++){
		if( (l_idx!=DArray_count(new_left) && cmp(new_left->contents[l_idx],new_right->contents[r_idx])<=0)
			       	|| r_idx==DArray_count(new_right)){
			DArray_push(array_sorted,new_left->contents[l_idx++]);
		}
		else {
			DArray_push(array_sorted,new_right->contents[r_idx++]);
		}
	}*/
	//free(temp);
	return 0;
error:
	return -1;
}
