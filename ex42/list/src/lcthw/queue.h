#ifndef lcthw_Queue_h
#define lcthw_Queue_h

#include "dbg.h"
#include <string.h>
#include <assert.h>
typedef struct QueueNode 
{
	void *value;
	struct QueueNode *next;
}QueueNode;
typedef struct Queue
{
	int size;
	struct QueueNode *front;
	struct QueueNode *rear;
}Queue;

#define QUEUE_FOREACH(L, V) QueueNode *V=NULL;\
    for(V = L->front; V != NULL; V = V->next)
Queue* Queue_create()
{
	return calloc(1, sizeof(Queue));
}
void Queue_destroy(Queue *queue)
{
	assert(queue != NULL);
	QueueNode *temp=NULL;
	QUEUE_FOREACH(queue, cur){
		if(temp==NULL)
			temp=cur;
		else {
			free(temp);
			temp=cur;
		}
	}
	if(temp)
		free(temp);
	if(queue)
		free(queue);
	queue=NULL;
}
int Queue_count(Queue *queue)
{
	return queue->size;
}
void* Queue_recv(Queue *queue)
{
	QueueNode *temp;
	char *temp_str;
	if(queue->front==NULL)
		return 0;
	else {
		temp=queue->front;
		temp_str=temp->value;
		queue->front=(queue->front)->next;
		queue->size--;
		free(temp);
	}		
	return temp_str;
}
void Queue_send(Queue *queue, void *str)
{
	QueueNode* cur = calloc(1,sizeof(QueueNode));
	cur->value=str;
	cur->next=NULL;
	if(queue->front==NULL){
		queue->front=cur;
		queue->rear=cur;
	} else{
		(queue->rear)->next = cur;
		queue->rear=cur;
	}
	queue->size++;
}
void* Queue_peek(Queue *queue)
{
	if(queue->front!=NULL)
		return (queue->front)->value;
	else 
		return NULL;
}
#endif
