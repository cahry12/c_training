#ifndef lcthw_Stack_h
#define lcthw_Stack_h

#include "dbg.h"
#include <string.h>
#include <assert.h>
typedef struct StackNode 
{
	void *value;
	struct StackNode *next;
}StackNode;
typedef struct Stack
{
	int size;
	struct StackNode *top;
}Stack;

#define STACK_FOREACH(L, V) StackNode *V=NULL;\
    for(V = L->top; V != NULL; V = V->next)
Stack* Stack_create()
{
	return calloc(1, sizeof(Stack));
}
void Stack_destroy(Stack *stack)
{
	assert(stack != NULL);
	StackNode *temp=NULL;
	STACK_FOREACH(stack, cur){
		if(temp==NULL)
			temp=cur;
		else {
			free(temp);
			temp=cur;
		}
	}
	if(temp)
		free(temp);
	if(stack)
		free(stack);
	stack=NULL;
}
int Stack_count(Stack *stack)
{
	/*
  	int size = 0;
	STACK_FOREACH(stack, cur){
		++size;
	}

 	return size; */
	return stack->size;
}
void* Stack_pop(Stack *stack)
{
	assert(stack->top!=NULL);
	StackNode *temp=stack->top;
	char *temp_str=temp->value;
	stack->top=temp->next;
	stack->size--;
	free(temp);
	return temp_str;
}
void Stack_push(Stack *stack, void *str)
{
	StackNode* cur = calloc(1,sizeof(StackNode));
	cur->value=str;
	cur->next=stack->top;
	stack->top=cur;
	stack->size++;
}
void* Stack_peek(Stack *stack)
{
	assert(stack->top!=NULL);
	return (stack->top)->value;
}

#endif
