#ifndef lcthw_Stack_Darray_h
#define lcthw_Stack_Darray_h
#define DEFAULT_EXPAND_RATE 300
#include "dbg.h"
#include <string.h>
#include <assert.h>
typedef struct Stack
{
	int end;
        int max;
	size_t element_size;
	size_t expand_rate;
	void **contents;	
}Stack;
#define STACK_COUNT(A) ((A)->end+1) 
#define STACK_FOREACH(L)\
    for(int idx=0 ; idx<STACK_COUNT(L); idx++)
Stack* Stack_create(size_t element_size, size_t initial_max)
{
	assert(initial_max>0);
	Stack *stack=malloc(sizeof(Stack));
	check_mem(stack);
	stack->max=initial_max;
	check(stack->max>0,"Stack's max should set more than zero");
	stack->contents=calloc(initial_max,sizeof(void *));
	check_mem(stack->contents);

	stack->end=-1;
	stack->element_size = element_size;
	stack->expand_rate = DEFAULT_EXPAND_RATE;
	return stack;
error:
	if(stack)free(stack);
	return NULL;
}
void Stack_destroy(Stack *stack)
{
	if(stack){
		if(stack->contents)free(stack->contents);
		free(stack);
	}
}
int Stack_count(Stack *stack)
{
	return STACK_COUNT(stack);
}
void* Stack_pop(Stack *stack)
{
	if(stack->end>=0)
		return stack->contents[stack->end--];
	else
		return NULL;
}
void Stack_push(Stack *stack, void *str)
{
	stack->contents[++stack->end]=str;
}
void* Stack_peek(Stack *stack)
{
	assert(stack->end>=0);
	return stack->contents[stack->end];
}

#endif
