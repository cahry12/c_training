#ifndef lcthw_Queue_Darray_h
#define lcthw_Queue_Darray_h
#define DEFAULT_EXPAND_RATE 300
#include "dbg.h"
#include <string.h>
#include <assert.h>
typedef struct Queue
{
	int front;
	int rear;
        int max;
	size_t element_size;
	size_t expand_rate;
	void **contents;	
}Queue;

#define QUEUE_FOREACH(L)\
    for(int idx=L->front ; idx< L->rear; idx++)
Queue* Queue_create(size_t element_size, size_t initial_max)
{
	assert(initial_max>0);
	Queue *queue=malloc(sizeof(Queue));
	check_mem(queue);
	queue->max=initial_max;
	check(queue->max>0,"Queue's max should set more than zero");
	queue->contents=calloc(initial_max,sizeof(void *));
	check_mem(queue->contents);

	queue->front=-1;
	queue->rear=-1;
	queue->element_size = element_size;
	queue->expand_rate = DEFAULT_EXPAND_RATE;
	return queue;
error:
	if(queue)free(queue);
	return NULL;
}
void Queue_destroy(Queue *queue)
{
	if(queue){
		if(queue->contents)free(queue->contents);
		free(queue);
	}
}
int Queue_count(Queue *queue)
{
	int size=0;
	for(int idx=queue->front;idx<=queue->rear;idx++,size++);
	return size;
}
void* Queue_recv(Queue *queue)
{
	if(!(queue->front==-1 || queue->front> queue->rear))
		return queue->contents[queue->front++];
	else
		return NULL;
}
void Queue_send(Queue *queue, void *str)
{
	assert(queue->front!=(queue->max-1));
	if(queue->front==-1)
		queue->front=0;
	queue->contents[++queue->rear]=str;
}
void* Queue_peek(Queue *queue)
{
	assert(queue->front>=0);
	return queue->contents[queue->front];
}

#endif
