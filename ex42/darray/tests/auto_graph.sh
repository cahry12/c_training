(
echo 'set output "tests/perforamnce_sorting_with_different_size.png"'
echo 'set terminal png'
echo 'set xlabel "The size of elements"'
echo 'set ylabel "Avg. processing time(sec)"'
echo 'set xrange [ 10:5000 ]'
echo 'set style data lines' 
echo 'set grid'
echo 'plot "tests/bubble_data_darray_ds" using 1:2 title "Bubble sort(darray)" with linespoints, \'
echo ' "tests/bubble_data_list_ds" using 1:2 title "Bubble sort(link list)" with linespoints, \'
echo ' "tests/merge_data_darray_ds" using 1:2 title "Merge sort(darray)" with linespoints, \'
echo ' "tests/merge_data_list_ds" using 1:2 title "Merge sort(link list)" with linespoints'

echo 'q'
) | gnuplot
