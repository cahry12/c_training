#include <stdio.h>
#include <ctype.h>
#include "dbg.h"
#include "libex29_credit4.h"

void test_credit();
int test_call_credit(const char *msg)
{
	printf("A STRING: %s\n",msg);
	test_credit();
	//printf("a value is %d\n",a);
	return 0;
}
int print_a_message(const char *msg)
{
    printf("A STRING: %s\n", msg);

    return 0;
}

int uppercase(const char *msg)
{
    int i = 0;

    // BUG: \0 termination problems
    for(i = 0; msg[i] != '\0'; i++) {
        printf("%c", toupper(msg[i]));
    }

    printf("\n");

    return 0;
}

int lowercase(const char *msg)
{
    int i = 0;

    // BUG: \0 termination problems
    for(i = 0; msg[i] != '\0'; i++) {
        printf("%c", tolower(msg[i]));
    }

    printf("\n");

    return 0;
}

int fail_on_purpose(const char *msg)
{
    return 1;
}

