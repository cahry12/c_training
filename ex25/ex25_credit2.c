#include <string.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include "dbg.h"

#define MAX_DATA 100
void myprintf(const char *format, ...)
{
	int i;
	const char *p;
	va_list args;
	char* s;
	//char formatbuf[256];
	va_start(args,format);
	for(p=format; *p!='\0'; p++){
		if(*p!='%'){
			putchar(*p);
			continue;
		}
		switch(*++p){
			case 'c':
				i = va_arg(args, int);
				putchar(i);
				break;
			case 'd':
				i = va_arg(args, int);
				//itoa(i,formatbuf,10);
				sprintf(s,"%d",i);
				fputs(s,stdout);
				break;
			case 's':
				s = va_arg(args, char*);
				fputs(s,stdout);
				break;
			case 'x':
				i = va_arg(args, int);
				//itoa(i,formatbuf,16);
				sprintf(s, "%x",i);
				fputs(s,stdout);
				break;
			case '%':
				putchar('%');
				break;
		}
	}
	va_end(args);
}
int read_string(char **out_string, int max_buffer)
{
	*out_string = calloc(1,max_buffer+1);
	check_mem(*out_string);
	char *result = fgets(*out_string, max_buffer, stdin);
	check(result!=NULL,"Input error.");
	return 0;
error:
	if(*out_string)free(*out_string);
	*out_string = NULL;
	return -1;
}
int read_int(int *out_int)
{
	char *input=NULL;
	int rc = read_string(&input,MAX_DATA);
	check(rc==0,"Failed to read number.");
	*out_int = atoi(input);
	free(input);
	input=NULL;
	return 0;
error:
	if(input)free(input);
	input = NULL;
	return -1;
}
int read_scan(const char *fmt, ...)
{
	int i = 0;
	int rc = 0;
	int *out_int = NULL;
	char *out_char = NULL;
	char **out_string = NULL;
	int max_buffer = 0;
	va_list argp;
	va_start(argp, fmt);

	for(i=0;fmt[i]!='\0';++i) {
		if(fmt[i]=='%'){
			i++;
			switch(fmt[i]){
				case '\0':
					sentinel("Invalid format, you ended with %%.");
					break;
				case 'd':
					out_int = va_arg(argp,int *);
					rc = read_int(out_int);
					check (rc==0,"Failed to read it.");
					break;
				case 'c':
					out_char = va_arg(argp,char *);
					*out_char = fgetc(stdin);
					break;
				case 's':
					max_buffer = va_arg(argp,int);
					out_string = va_arg(argp,char **);
					rc = read_string(out_string, max_buffer);
					check(rc==0, "Failed to read it.");
					break;
				default:
					sentinel("Invalid format.");
			}
		} else{
			fgetc(stdin);
		}
		check(!feof(stdin) && !ferror(stdin),"Input error");
	}
	va_end(argp);
	return 0;
error:
	va_end(argp);
	return -1;
}
int main(int argc,char *argv[])
{
	char *first_name = NULL;
	char initial = ' ';
	char *last_name = NULL;
	int age = 0;

	myprintf("What's your first name?");
	int rc = read_scan("%s",MAX_DATA,&first_name);
	check(rc==0,"Failed first name");
	myprintf("What's your initial?");
	rc=read_scan("%c\n",&initial);
	check(rc==0, "Failed initial");
	myprintf("What's your last name?");
	rc = read_scan("%s",MAX_DATA,&last_name);
	check(rc==0,"Failed last name");

	myprintf("How old are you? ");
	rc = read_scan("%d",&age);


	myprintf("---- RESULTS ----\n");
	myprintf("First Name: %s\n",first_name);
	myprintf("Initial: '%c'\n",initial);
	myprintf("Last Name: %s\n",last_name);
	myprintf("Age: %d\n",age);
	free(last_name);
	free(first_name);
	return 0;
error:
	return -1;
}

