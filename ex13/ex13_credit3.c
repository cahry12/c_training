#include<stdio.h>
#include<stdlib.h>
#define TOLOWER(C) C= (C>='A' && C<='Z') ? (C+32) : C
int main(int argc,char *argv[])
{
	int i,j;
	for(j=0; j<argc ;j++){
	for(i=0; argv[j][i] != '\0'; ++i){
		char letter = argv[j][i];
			switch(letter) {
				case 'a':
					printf("%d: 'A'\n",i);
					break;
				case 'e':
					printf("%d: 'E'\n",i);
					break;
				case 'i':
					printf("%d: 'I'\n",i);
					break;
				case 'o':
					printf("%d: 'O'\n",i);
					break;
				case 'u':
					printf("%d: 'U'\n",i);
					break;
				case 'y':
					if(i>2){
						printf("%d: 'Y'\n",i);
					}
					break;
				default:
					printf("%d: %c is not a vowel\n",i, letter);
			}
		}
	}
	return 0;
}
