#include <lcthw/list_algos.h>
#include <lcthw/dbg.h>

inline void listNode_swap(ListNode *a,ListNode *b)
{
	void *temp=a->value;
	a->value=b->value;
	b->value=b->value;
	return;
}

int List_bubble_sort(List *list,List_compare cmp)
{
	if(List_count(list)<=1)
		return 0;
	COUNT_FOREACH(List_count(list)){
		LIST_FOREACH(list,first,next,cur){
			if(cur->next){
				if(cmp(cur->value,cur->next->value)>0){
					listNode_swap(cur,cur->next);
				}
			}
		}
	}	
	return 0;
}
List *List_merge_sort(List *list,List_compare cmp)
{
	if(List_count(list)<=1)
		return 0;
	List *left = List_create();
	List *right = List_create();
	int middle = List_count(list)/2;// 計算中心值(middle value)用來插入左/右部分鏈結跟排序左/右部分鏈結

	//插入左部分鏈結和右部分鏈結--begin
	int idx=0;
	LIST_FOREACH(list,first,next,cur){
		if(idx<middle){
			List_push(left,cur->value);
		}
		else {
			List_push(right,cur->value);
		}
	}
	//插入左部分鏈結與又部分鏈結--end

}
