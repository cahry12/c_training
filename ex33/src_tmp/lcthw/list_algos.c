#include <lcthw/list_algos.h>
#include <lcthw/dbg.h>

inline void listNode_swap(ListNode *a,ListNode *b)
{
	void *temp=a->value;
	a->value=b->value;
	b->value=temp;
	return;
}

int List_bubble_sort(List *list,List_compare cmp)
{
	if(List_count(list)<=1)
		return 0;
	COUNT_FOREACH(List_count(list)){
		LIST_FOREACH(list,first,next,cur){
			if(cur->next){
				if(cmp(cur->value,cur->next->value)>0){
					listNode_swap(cur,cur->next);
				}
			}
		}
	}	
	return 0;
}
List *List_merge_sort(List *list,List_compare cmp)
{
	if(List_count(list)<=1)
		return list;
		//	return 0;  //警告！這裏不能回傳0否則會錯誤
	List *left = List_create();
	List *right = List_create();
	int middle = List_count(list)/2;// 計算中心值(middle value)用來插入左/右部分鏈結跟排序左/右部分鏈結

	//插入左部分鏈結和右部分鏈結(begin)
	int idx=0;
	LIST_FOREACH(list,first,next,cur){
		if(idx<middle){
			List_push(left,cur->value);
		}
		else {
			List_push(right,cur->value);
		}
		idx++;
	}
	//插入左部分鏈結與右部分鏈結(end)
	//透過遞迴方式去切割(divid)並且於切到為1時return 0來準備開始做下一步sorted(conquer)--begin//
	List *left_new=List_merge_sort(left,cmp);
	List *right_new=List_merge_sort(right,cmp);
	//透過遞迴方式去切割(divid)並且於切到為1時return 0來準備開始做下一步sorted(conquer)--end//
	// 開始透過merge(conquer 一一排序(begin) //
	List *list_sorted=List_create();
	void *sorted_value;
	while(List_count(left_new)>0 && List_count(right_new)>0){
		if(cmp(List_first(left_new), List_first(right_new)) <= 0) { //代表左成員的值<右成員的值或是兩成員的值相同
			sorted_value=List_shift(left_new);
		}
		else {
			sorted_value=List_shift(right_new);
		}
		List_push(list_sorted,sorted_value);
	}
	while(List_count(left_new)>0){
		sorted_value=List_shift(left_new);
		List_push(list_sorted,sorted_value);
	}
	while(List_count(right_new)>0){
		sorted_value=List_shift(right_new);
		List_push(list_sorted,sorted_value);
	}
	// 開始透過merge 一一排序(end)//
	return list_sorted;  //回傳已sort list

}/*
List * List_merge_sort_bottomup(List *list, List_compare cmp)
{

}*/
