#include <lcthw/list_algos.h>
#include <lcthw/dbg.h>
inline void listNode_swap(ListNode *a,ListNode *b)
{
	void *temp=a->value;
	a->value=b->value;
	b->value=temp;
	return;
}

int List_bubble_sort(List *list,List_compare cmp)
{
	if(List_count(list)<=1)
		return 0;
	COUNT_FOREACH(List_count(list)){
		LIST_FOREACH(list,first,next,cur){
			if(cur->next){
				if(cmp(cur->value,cur->next->value)>0){
					listNode_swap(cur,cur->next);
				}
			}
		}
	}	
	return 0;
}
List *List_merge_sort(List *list,List_compare cmp)
{
	if(List_count(list)<=1)
		return list;
		//	return 0;  //警告！這裏不能回傳0否則會錯誤
	List *left = List_create();
	List *right = List_create();
	int middle = List_count(list)/2;// 計算中心值(middle value)用來插入左/右部分鏈結跟排序左/右部分鏈結

	//插入左部分鏈結和右部分鏈結(begin)
	int idx=0;
	LIST_FOREACH(list,first,next,cur){
		if(idx<middle){
			List_push(left,cur->value);
		}
		else {
			List_push(right,cur->value);
		}
		idx++;
	}
	//插入左部分鏈結與右部分鏈結(end)
	//透過遞迴方式去切割(divid)並且於切到為1時return 0來準備開始做下一步sorted(conquer)--begin//
	List *left_new=List_merge_sort(left,cmp);
	List *right_new=List_merge_sort(right,cmp);
	//透過遞迴方式去切割(divid)並且於切到為1時return 0來準備開始做下一步sorted(conquer)--end//
	// 開始透過merge(conquer 一一排序(begin) //
	List *list_sorted=List_create();
	void *sorted_value;
	while(List_count(left_new)>0 && List_count(right_new)>0){
		if(cmp(List_first(left_new), List_first(right_new)) <= 0) { //代表左成員的值<右成員的值或是兩成員的值相同
			sorted_value=List_shift(left_new);
		}
		else {
			sorted_value=List_shift(right_new);
		}
		List_push(list_sorted,sorted_value);
	}
	while(List_count(left_new)>0){
		sorted_value=List_shift(left_new);
		List_push(list_sorted,sorted_value);
	}
	while(List_count(right_new)>0){
		sorted_value=List_shift(right_new);
		List_push(list_sorted,sorted_value);
	}
	free(left);
	free(right);
	// 開始透過merge 一一排序(end)//
	return list_sorted;  //回傳已sort list

}
List* List_merge_bottomup_sort(List *list,List_compare cmp)
{
	if(List_count(list)<=1)
		return list;
	List *left = List_create();
	List *right = List_create();
	List *old_list =NULL;
	ListNode *node=NULL;
	void *sorted_value;
	WIDTH_DOREACH(List_count(list)){  //根據link list長度來去做bottom-up merge sort
		List *list_sorted = List_create();
		int left_idx, right_idx;
		for(node=list->first,left_idx=0;left_idx<List_count(list);left_idx=left_idx+2*width){
			right_idx = MIN(left_idx + width,List_count(list)-1);
			left->first=node,right->first=node;
			int l_width=0,r_width=0;
			//設定左半部長度
			int left_idx_temp=left_idx,right_idx_temp=right_idx;
			while(l_width<width){
				if(left_idx_temp==(List_count(list)))break;
				l_width++;
				left_idx_temp++;
			}
			//設定右半部長度
			if(left_idx_temp<List_count(list)){
				while(r_width<width){
				//printf("QQ-%d,%d\n",right_idx_temp,List_count(list));
				if(right_idx_temp==(List_count(list)))break;
					r_width++;
					right_idx_temp++;
				}
			}
			//指向右半部begin
			node=list->first;
			for(int init_idx=0;init_idx<left_idx;init_idx++){
                                node=node->next;        //跳到左部分第一 index 指定的位址(不像array可以透過index直接指向...
			}
			left->first=node;
			//指向左半部end
			//指向右半部begin
			node=list->first;
			for(int init_idx=left_idx;init_idx<right_idx;init_idx++){
                                node=node->next;      //跳到右部分第一 index 指定的位址(不像array可以透過index直接指向...
			}
			right->first=node;
			//指向右半部end
			int l_len=0,r_len=0;
			ListNode *leftNode=left->first,*rightNode=right->first;
			//printf("l_width=%d,r_width=%d,l_len=%d,r_len=%d,width=%d\n",l_width,r_width,l_len,r_len,width);
			//printf("Left's element:%d,Right's element:%d\n",l_width,r_width);
			while(l_len<l_width && r_len < r_width){
				if(cmp(leftNode->value,rightNode->value) <0){
					sorted_value=leftNode->value;
					leftNode=leftNode->next;
					l_len++;
				}else{
					sorted_value=rightNode->value;
					rightNode=rightNode->next;
					r_len++;
				}
				List_push(list_sorted,sorted_value);
			}
			while(l_len<l_width){
				sorted_value=leftNode->value;
				leftNode=leftNode->next;
				List_push(list_sorted,sorted_value);
				l_len++;
			}
			while(r_len<r_width){
				sorted_value=rightNode->value;
				rightNode=rightNode->next;
				List_push(list_sorted,sorted_value);
				r_len++;
			}
		}//for-loop
		list=list_sorted;
		if(old_list==NULL)
			old_list=list_sorted;
		else{
			List_destroy(old_list);//釋放上一for-loop中list_sorted所產生的記憶體資源
			old_list=list_sorted;
		}
	}//WIDTH_FOREACH
	free(left);
	free(right);
	return list;
	// 開始透過merge 一一排序(end)//
}
/*
List* List_merge_sort_bottomup(List *list,List_compare cmp)
{
	if(List_count(list)<=1)
		return list;
		//	return 0;  //警告！這裏不能回傳0否則會錯誤
	List *left = List_create();
	List *right = List_create();
int idx=0;
	ListNode *node=calloc(1,sizeof(ListNode));
	WIDTH_DOREACH(List_count(list)){  //根據link list長度來去做bottom-up merge sort
		for(node=list->first,idx=0;idx<list_count(list);idx=idx+2*width){
			int middle=idx+2*width/2;	
			int left_idx=idx,right_idx=middle;
			List *left=node, *right=node;
			for(int init_idx=0;init_idx<left_idx;init_idx++)
				left=left->next;	//跳到左部分第一 index 指定的位址(不像array可以透過index直接指向...
			for(int init_idx=0;init_idx<right_idx;init_idx++)
				right=right->next;	//跳到右部分第一 index 指定的位址(不像array可以透過index直接指向...
			for(int j=left_idx;j<(idx+2*width);j++){
				if(cmp(left->value,right->value)>0){
					right=right->next;
				} else{
					left=left->next; //相當於i0=i0+1;
					left=left->next;
				}
			}
		}
	}
	//插入左部分鏈結和右部分鏈結(begin)
	int idx=0;
	LIST_FOREACH(list,first,next,cur){
		if(idx<middle){
			List_push(left,cur->value);
		}
		else {
			List_push(right,cur->value);
		}
		idx++;
	}
	//插入左部分鏈結與右部分鏈結(end)
	//透過遞迴方式去切割(divid)並且於切到為1時return 0來準備開始做下一步sorted(conquer)--begin//
	List *left_new=List_merge_sort_bottomup(left,cmp);
	List *right_new=List_merge_sort_bottomup(right,cmp);
	//透過遞迴方式去切割(divid)並且於切到為1時return 0來準備開始做下一步sorted(conquer)--end//
	// 開始透過merge(conquer 一一排序(begin) //
	List *list_sorted=List_create();
	void *sorted_value;
	while(List_count(left_new)>0 && List_count(right_new)>0){
		if(cmp(List_first(left_new), List_first(right_new)) <= 0) { //代表左成員的值<右成員的值或是兩成員的值相同
			sorted_value=List_shift(left_new);
		}
		else {
			sorted_value=List_shift(right_new);
		}
		List_push(list_sorted,sorted_value);
	}
	while(List_count(left_new)>0){
		sorted_value=List_shift(left_new);
		List_push(list_sorted,sorted_value);
	}
	while(List_count(right_new)>0){
		sorted_value=List_shift(right_new);
		List_push(list_sorted,sorted_value);
	}
	// 開始透過merge 一一排序(end)//
	return list_sorted;  //回傳已sort list

}
*/
List * List_insert_sorted(List *list, void * value, List_compare cmp)
{
/// 方法一：先排序參數的link list後再協助宣告node並且設定value後從尾端依照大小順序插入至list_sorted的適當位置
	List *list_sorted_init=List_merge_sort(list,cmp);  //先sort原先的list 
	List *list_sorted=list_sorted_init;		   //把sort過第一階段的list位址指派給list_sorted做第二階段sort
	ListNode *node = calloc(1,sizeof(ListNode));
	check_mem(node);

	node->value = value;
	node->prev = NULL;
	node->next = NULL;
	if(list_sorted==NULL){
		list_sorted->first=node;
		list_sorted->last=node;
	} else{
		ListNode *search=list_sorted->last;
		while(search){
			if(cmp(search->value,node->value)<=0)
				break;
			search=search->prev;
		}
		node->next=search->next;		
		search->next=node;
		node->prev=search;

	}
	return list_sorted;
error:
	return list_sorted;
}
