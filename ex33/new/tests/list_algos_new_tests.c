#include "minunit.h"
#include <lcthw/list_algos.h>
#include <assert.h>
#include <string.h>
#include <time.h>
char *values[] = {"XXXX", "1234", "abcd", "xjvef", "NDSS"};
char *insert_str = "WCT";
#define NUM_VALUES 5
#define EXCUTE_TIMES 10000
#define STR_LEN 10
#define NEW_NUM_VALUES 10000
//#define DEBUG
//#define LIST_SIZE 500
#ifdef DEBUG
int global_idx=0;
#endif
void random_gen_str(char *s, const int len) {
    const char alpha_and_num[] =
        "0123456789"
        "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
        "abcdefghijklmnopqrstuvwxyz";

    for (int i = 0; i < len; ++i) {
        s[i] = alpha_and_num[rand() % (sizeof(alpha_and_num) - 1)];
    }
    s[len] = '\0';
#ifdef DEBUG
    printf("[GENERATING STRING AT INDEX %d,len=%d]=>",++global_idx,len);
    puts(s);
#endif
}
List *create_words_by_gen(int size)
{
	List *words = List_create();
	char *auto_str=NULL;
	int i,len;
	for(i=0;i<size;i++){
		len = (rand() % STR_LEN)+1;
		if(len==1)len++;
		auto_str = malloc(sizeof(len));
		random_gen_str(auto_str,len-1);
		List_push(words,auto_str);
	}
	return words;
}
List *create_words()
{
	int i=0;
	List *words = List_create();
#ifdef LIST_SIZE
	int ct=0;
	while(ct<LIST_SIZE){
		for(i=0;i<NUM_VALUES;i++){
			List_push(words, values[i]);
		}
	ct+=5;
	}
#else
	for(i=0;i<NUM_VALUES;i++){
			List_push(words, values[i]);
	}
#endif

	return words;
}
int is_sorted(List *words)
{
	LIST_FOREACH(words, first, next, cur) {
		if(cur->next && strcmp(cur->value, cur->next->value) > 0){
			debug("%s %s",(char *)cur->value, (char *)cur->next->value);
			return 0;
		}
	}
	return 1;
}
char *test_bubble_sort()
{
	List *words = create_words();
	int rc = List_bubble_sort(words,(List_compare)strcmp);
	mu_assert(rc==0,"Bubble sort failed");
	mu_assert(is_sorted(words),"Words aren't sorted after bubble sort.");
	
	rc = List_bubble_sort(words, (List_compare)strcmp);
	mu_assert(rc ==0,"Bubble sort of already sorted failed");
	mu_assert(is_sorted(words),"Words should be sort if already bubble sorted.");

	List_destroy(words);
	
	words = List_create(words);
	rc = List_bubble_sort(words, (List_compare)strcmp);
	mu_assert(rc == 0,"Bubble sort failed on empty list.");
	mu_assert(is_sorted(words),"Words should be sorted if empty.");
	List_destroy(words);


	return NULL;
}
char *test_merge_sort()
{
	List *words = create_words();
	List *res = List_merge_sort(words, (List_compare)strcmp);
	mu_assert(is_sorted(res),"Words aren't sorted after merge sort.");

	List *res2 = List_merge_sort( res, (List_compare)strcmp);
	mu_assert(is_sorted(res), "Should still be sorted after merge sort.");

	List_destroy(res2);
	List_destroy(res);
	List_destroy(words);
	return NULL;
}
char *test_merge_bottomup_sort()
{
	List *words = create_words();
	//mu_assert(is_sorted(words),"words aren't sorted.");
	List *res = List_merge_bottomup_sort(words, (List_compare)strcmp);
	mu_assert(is_sorted(res),"Words aren't sorted after merge sort.");

	List *res2 = List_merge_bottomup_sort( res, (List_compare)strcmp);
	mu_assert(is_sorted(res), "Should still be sorted after merge sort.");
	//printf("測試13\n");
	List_destroy(res2);
	List_destroy(res);
	List_destroy(words);
	return NULL;
}
char *test_merge_and_bubble_performance()
{
	clock_t begin,end;
	double avg_processing_time=0.0;
	FILE *merge_file=fopen("tests/merge_data","a");
	FILE *bubble_file=fopen("tests/bubble_data","a");

	mu_assert(merge_file,"Create merge file failed!");
	mu_assert(bubble_file,"Create bubble file failed!");
	int run_times=100;
	int i,rc;
	List *words=NULL, *res=NULL;
	while(run_times<=EXCUTE_TIMES){
		i=0;
		while(i++<run_times){
			words = create_words();
			begin=clock();
			res= List_merge_sort(words,(List_compare)strcmp);
			end=clock();
			mu_assert(is_sorted(res),"Words aren't sorted after merge sort.");
			avg_processing_time+=(double)(end-begin)/ CLOCKS_PER_SEC;
			List_destroy(words);
			List_destroy(res);
		}
	//	printf("Avg. excute time for merge sort: %.10f sec\n",avg_processing_time/(double)run_times);
		fprintf(merge_file,"%d %.10f\n",run_times,avg_processing_time/(double)run_times);
		i=0;avg_processing_time=0.0;
		while(i++<run_times){
			words = create_words();
			begin=clock();
			rc= List_bubble_sort(words,(List_compare)strcmp);
			end=clock();
			mu_assert(rc==0,"Words aren't sorted after bubble sort.");
			avg_processing_time+=(double)(end-begin)/ CLOCKS_PER_SEC;
			List_destroy(words);
		}
		fprintf(bubble_file,"%d %.10f\n",run_times,avg_processing_time/(double)run_times);
	//	printf("Avg. excute time for bubble sort: %.10f sec\n",avg_processing_time/(double)run_times);
		avg_processing_time=0.0;
		run_times+=200;
	}
	fclose(merge_file);
	fclose(bubble_file);
	return NULL;
}
char *test_merge_and_bubble_sort_performance()
{
	clock_t begin,end;
	double avg_processing_time=0.0;
	FILE *merge_file=fopen("tests/merge_data_s","a");
	FILE *bubble_file=fopen("tests/bubble_data_s","a");

	mu_assert(merge_file,"Create merge file failed!");
	mu_assert(bubble_file,"Create bubble file failed!");
	int run_times=100;
	int i,rc;
	int size=1000;
	List *words=NULL, *res=NULL;
	while(run_times<=EXCUTE_TIMES){
		i=0;
		while(i++<run_times){
			words = create_words_by_gen(size);
			begin=clock();
			res= List_merge_sort(words,(List_compare)strcmp);
			end=clock();
			mu_assert(is_sorted(res),"Words aren't sorted after merge sort.");
			avg_processing_time+=(double)(end-begin)/ CLOCKS_PER_SEC;
			List_destroy(words);
			List_destroy(res);
		}
	//	printf("Avg. excute time for merge sort: %.10f sec\n",avg_processing_time/(double)run_times);
		fprintf(merge_file,"%d %.10f\n",run_times,avg_processing_time/(double)run_times);
		i=0;avg_processing_time=0.0;
		while(i++<run_times){
			words = create_words_by_gen(size);
			begin=clock();
			rc= List_bubble_sort(words,(List_compare)strcmp);
			end=clock();
			mu_assert(rc==0,"Words aren't sorted after bubble sort.");
			avg_processing_time+=(double)(end-begin)/ CLOCKS_PER_SEC;
			List_destroy(words);
		}
		fprintf(bubble_file,"%d %.10f\n",run_times,avg_processing_time/(double)run_times);
	//	printf("Avg. excute time for bubble sort: %.10f sec\n",avg_processing_time/(double)run_times);
		avg_processing_time=0.0;
		run_times+=200;
	}
	fclose(merge_file);
	fclose(bubble_file);
	return NULL;
}
char *test_different_size_merge_and_bubble_performance()
{
	clock_t begin,end;
	double avg_processing_time=0.0;
	FILE *merge_file=fopen("tests/merge_data_ds","a");
	FILE *bubble_file=fopen("tests/bubble_data_ds","a");

	mu_assert(merge_file,"Create merge file failed!");
	mu_assert(bubble_file,"Create bubble file failed!");
	int run_times=10;
	int i=0,rc;
	List *words=NULL, *res=NULL;
	int size=10;
	while(size<NEW_NUM_VALUES){
		while(i++<run_times){
			words = create_words_by_gen(size);
			begin=clock();
			res= List_merge_sort(words,(List_compare)strcmp);
			end=clock();
			mu_assert(is_sorted(res),"Words aren't sorted after merge sort.");
			avg_processing_time+=(double)(end-begin)/ CLOCKS_PER_SEC;
			List_destroy(words);
			List_destroy(res);
		}
	//	printf("Avg. excute time for merge sort: %.10f sec\n",avg_processing_time/(double)run_times);
		fprintf(merge_file,"%d %.10f\n",size,avg_processing_time/(double)run_times);
		i=0;avg_processing_time=0.0;
		while(i++<run_times){
			words = create_words_by_gen(size);
			begin=clock();
			rc= List_bubble_sort(words,(List_compare)strcmp);
			end=clock();
			mu_assert(rc==0,"Words aren't sorted after bubble sort.");
			avg_processing_time+=(double)(end-begin)/ CLOCKS_PER_SEC;
			List_destroy(words);
		}
		fprintf(bubble_file,"%d %.10f\n",size,avg_processing_time/(double)run_times);
	//	printf("Avg. excute time for bubble sort: %.10f sec\n",avg_processing_time/(double)run_times);
		size+=100;
	}
	fclose(merge_file);
	fclose(bubble_file);
	return NULL;
}
char *test_different_size_merge_and_mergebottomup_performance()
{
	clock_t begin,end;
	double avg_processing_time=0.0;
	FILE *merge_file=fopen("tests/mergetopup_data_ds","a");
	FILE *bubble_file=fopen("tests/mergebottomup_data_ds","a");

	mu_assert(merge_file,"Create merge file failed!");
	mu_assert(bubble_file,"Create bubble file failed!");
	int run_times=10;
	int i=0;
	List *words=NULL, *res=NULL;
	int size=10;
	while(size<NEW_NUM_VALUES){
		while(i++<run_times){
			words = create_words_by_gen(size);
			begin=clock();
			res= List_merge_sort(words,(List_compare)strcmp);
			end=clock();
			mu_assert(is_sorted(res),"Words aren't sorted after merge sort.");
			avg_processing_time+=(double)(end-begin)/ CLOCKS_PER_SEC;
			List_destroy(words);
			List_destroy(res);
		}
	//	printf("Avg. excute time for merge sort: %.10f sec\n",avg_processing_time/(double)run_times);
		fprintf(merge_file,"%d %.10f\n",size,avg_processing_time/(double)run_times);
		i=0;avg_processing_time=0.0;
		while(i++<run_times){
			words = create_words_by_gen(size);
			begin=clock();
			res= List_merge_bottomup_sort(words,(List_compare)strcmp);
			end=clock();
			mu_assert(is_sorted(res),"Words aren't sorted after merge sort.(bottom-up version)");
			avg_processing_time+=(double)(end-begin)/ CLOCKS_PER_SEC;
			List_destroy(words);
		}
		fprintf(bubble_file,"%d %.10f\n",size,avg_processing_time/(double)run_times);
	//	printf("Avg. excute time for bubble sort: %.10f sec\n",avg_processing_time/(double)run_times);
		size+=100;
	}
	fclose(merge_file);
	fclose(bubble_file);
	return NULL;
}
char *test_insert_sorted()
{
	List *words = create_words();
	List *res = List_merge_sort(words, (List_compare)strcmp);
	mu_assert(is_sorted(res),"Word aren't sorted after merge sort.");
	//insert and sort testing...[begin]
	List *res2 = List_insert_sorted(res, insert_str,(List_compare)strcmp);
	mu_assert(is_sorted(res2),"Word aren't sorted after inserting");
	//insert and sort testing...[end]
	return NULL;
}
char *all_tests()
{
	mu_suite_start();
	mu_run_test(test_bubble_sort);
	mu_run_test(test_merge_sort);
	mu_run_test(test_merge_bottomup_sort);
	mu_run_test(test_merge_and_bubble_performance);
	mu_run_test(test_different_size_merge_and_mergebottomup_performance);
	//u_run_test(test_merge_and_bubble_sort_performance);
	mu_run_test(test_different_size_merge_and_bubble_performance);
	mu_run_test(test_insert_sorted);
	return NULL;
}
RUN_TESTS(all_tests);
