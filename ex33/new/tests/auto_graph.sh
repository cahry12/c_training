(
echo 'set output "tests/perforamnce_sorting(link-list version).png"'
echo 'set terminal png'
echo 'set xlabel "The number of execution"'
echo 'set ylabel "Avg. processing time"'
echo 'set xrange [ 100:10000 ]'
echo 'set style data lines' 
echo 'set grid'
echo 'plot "tests/bubble_data" using 1:2 title "Bubble sort" with linespoints, \'
echo ' "tests/merge_data" using 1:2 title "Merge sort" with linespoints'

echo 'set output "tests/perforamnce_sorting_with_different_size(link-list version).png"'
echo 'set terminal png'
echo 'set xlabel "The size of link list"'
echo 'set ylabel "Avg. processing time"'
echo 'set xrange [ 10:10000 ]'
echo 'set style data lines' 
echo 'set grid'
echo 'plot "tests/bubble_data_ds" using 1:2 title "Bubble sort" with linespoints, \'
echo ' "tests/merge_data_ds" using 1:2 title "Merge sort" with linespoints'

echo 'set output "tests/perforamnce_sorting_with_different_size(Top-down_and_Bottom-up).png"'
echo 'set terminal png'
echo 'set xlabel "The size of link list"'
echo 'set ylabel "Avg. processing time"'
echo 'set xrange [ 10:10000 ]'
echo 'set style data lines' 
echo 'set grid'
echo 'plot "tests/mergetopup_data_ds" using 1:2 title "Merge sort(Top-down)" with linespoints, \'
echo ' "tests/mergebottomup_data_ds" using 1:2 title "Merge sort(Bottom-up)" with linespoints'
echo 'q'
) | gnuplot
