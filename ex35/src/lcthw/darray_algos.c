#include<lcthw/darray_algos.h>
#include<stdlib.h>

int DArray_qsort(DArray *array, DArray_compare cmp)
{
	qsort(array->contents, DArray_count(array), sizeof(void*),cmp);
	return 0;
}

int DArray_heapsort(DArray *array, DArray_compare cmp)
{
	return heapsort(array->contents, DArray_count(array), sizeof(void *),cmp);
}

int DArray_mergesort(DArray *array, DArray_compare cmp)
{
	return mergesort(array->contents, DArray_count(array),sizeof(void *),cmp);
}
int quick_sort(DArray *array, int left,int right,DArray_compare cmp){
	
	if(left>=right)
		return 1;
	void *pivot,*temp;
	int i,j;
	pivot = array->contents[left];
	i = left+1;
	j = right;

	do
	{
		while(i<right && cmp(&pivot,&array->contents[i])>=0)i++;
		while(j>left && cmp(&array->contents[j],&pivot)>0)j--;
		if(i<j){
			temp = array->contents[i]; array->contents[i]=array->contents[j]; array->contents[j]=temp;
		}
	}while(i<j);
	temp = array->contents[j];
	array->contents[j] = array->contents[left];
	array->contents[left] = temp;

	quick_sort(array, left, j-1,cmp);
	quick_sort(array, j+1, right,cmp);
	return 0;
}
int merge_sort(DArray *array,int left,int right,DArray_compare cmp){
	if(left < right){
		int middle = (left+right)/2;
                merge_sort(array,left,middle,cmp);
                merge_sort(array,middle+1,right,cmp);
		int l_idx=left,r_idx=middle+1,idx=0;
		void **temp=calloc(right,sizeof(void *));
#ifdef DEBUG
		printf("before sorting...\n");
		for(int idx=left;idx<=right;idx++){
			printf("%s ",array->contents[idx]);
		}
		puts("");
#endif
		while(l_idx<=middle && r_idx<=right) {
                        if(cmp(&array->contents[l_idx],&array->contents[r_idx])<=0){
                                temp[idx++]=array->contents[l_idx++];
                        }else {
                                temp[idx++]=array->contents[r_idx++];
                        }
                }
                while(l_idx<=middle){
                        temp[idx++]=array->contents[l_idx++];
                }
                while(r_idx<=right){
                        temp[idx++]=array->contents[r_idx++];
                }
                for(int idx=left,idx2=0;idx<=right;idx++,idx2++){
                        array->contents[idx]=temp[idx2];
			//temp[idx2]=NULL;
                }
#ifdef DEBUG
		printf("after sorting...\n");
		for(int idx=left;idx<=right;idx++){
			printf("%s ",array->contents[idx]);
		}
		puts("");
#endif
		//printf("after,left=%d,right=%d\n",left,right);
		//if(left != 0 && right != DArray_count(array)-1)
		//	free(temp);

	}
	return 0;
}
void max_heapify(DArray *array,int idx, int heap_size,DArray_compare cmp)
{
	int l = 2*idx+1;
	int r = 2*idx+2;
	int largest;
	void* temp;
	if(l < heap_size && cmp(&array->contents[l],&array->contents[idx])>0)
		largest=l;
	else 
		largest=idx;
	if(r < heap_size && cmp(&array->contents[r],&array->contents[largest])>0)
		largest=r;
	if(largest!=idx){
		temp = array->contents[idx];
		array->contents[idx]=array->contents[largest];
		array->contents[largest]=temp;
		max_heapify(array,largest,heap_size,cmp);
	}
}
void build_max_heap(DArray *array,int heap_size, DArray_compare cmp)
{
	for(int i=(heap_size-2)/2;i>=0;i--)
		max_heapify(array,i,heap_size,cmp);
}
int heap_sort(DArray *array,int heap_size, DArray_compare cmp)
{
	build_max_heap(array,heap_size,cmp);
	void* temp;
	for(int i = heap_size-1;i>=0;i--)
	{
		temp = array->contents[0];
		array->contents[0]=array->contents[i];
		array->contents[i]=temp;
		max_heapify(array,0,i,cmp);
	}
	return 0;
}
int Custom_DArray_qsort(DArray *array, DArray_compare cmp)
{
	check(array!=NULL,"array shouldn't be NULL!");
	return quick_sort(array,0,DArray_count(array)-1,cmp);
error:
	return -1;
}
int Custom_DArray_heapsort(DArray *array, DArray_compare cmp)
{
	check(array!=NULL,"array shouldn't be NULL!");
	return heap_sort(array,DArray_count(array),cmp);
error:
	return -1;
}
int Custom_DArray_mergesort(DArray *array, DArray_compare cmp)
{
	check(array!=NULL,"array shouldn't be NULL!");
	return merge_sort(array,0,DArray_count(array)-1,cmp);
error:
	return -1;
}
int DArray_sort_add(DArray *array,void **value,int len,DArray_compare cmp)
{
	check(array!=NULL,"array shouldn't be NULL!");
	int rc;
	rc = quick_sort(array, 0, DArray_count(array)-1,cmp);
	check(rc==0,"Occur error when executing sort method!");
	for(int i=0;i<len;i++)
		DArray_push(array,value[i]);
	rc = quick_sort(array, 0, DArray_count(array)-1,cmp);
	check(rc==0,"Occur error wheen executing sort method!");
	return 0;
error:
	return -1;
}
int DArray_find(DArray *array,void *to_find,DArray_compare cmp)
{
	check(array!=NULL,"array shouldn't be NULL!");
	int low = 0;
	int high = DArray_count(array)-1;
	while(low <= high){
		int middle = low +(high - low)/2;
		if(cmp(&array->contents[middle],&to_find)>0){
			high = middle - 1;
		} else if(cmp(&array->contents[middle],&to_find)<0){
			low = middle + 1;
		}else {
			return middle;//return index which matchs to_find requirement.
		}
	}
	return -1;
error:
	return -1;
}
