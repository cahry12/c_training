#ifndef darray_algos_h
#define darray_algos_h
#include <lcthw/darray.h>
#include <lcthw/dbg.h>
typedef int (*DArray_compare)(const void *a, const void *b);

int DArray_qsort(DArray *array, DArray_compare cmp);

int DArray_heapsort(DArray *array, DArray_compare cmp);

int DArray_mergesort(DArray *array, DArray_compare cmp);

int quick_sort(DArray *array,int left,int right, DArray_compare cmp);

int merge_sort(DArray *array,int left,int right, DArray_compare cmp);

void max_heapify(DArray *array,int idx, int heap_size,DArray_compare cmp);

void build_max_heap(DArray *array,int heap_size,DArray_compare cmp);

int heap_sort(DArray *array,int heap_size, DArray_compare cmp);

int Custom_DArray_qsort(DArray *array, DArray_compare cmp);

int Custom_DArray_heapsort(DArray *array, DArray_compare cmp);

int Custom_DArray_mergesort(DArray *array, DArray_compare cmp);

int DArray_sort_add(DArray *array,void **value,int len,DArray_compare cmp);

int DArray_find(DArray *array,void *to_find,DArray_compare cmp);
#endif
