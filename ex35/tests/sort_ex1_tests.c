#include "minunit.h"
#include <time.h>
#include <string.h>
#include <stdint.h>
#include <lcthw/darray_algos.h>
#define STR_LEN 10
#define NEW_NUM_VALUES 1010
#ifndef DEBUG
//#define DEBUG
int global_idx=0;
#endif

int testcmp(char **a, char **b)
{
	//printf("test,%s,%s",*a,*b);
    return strcmp(*a, *b);
}

DArray *create_words()
{
    DArray *result = DArray_create(0, 5);
    char *words[] = {"asdfasfd", "werwar", "13234", "asdfasfd", "oioj"};
    int i = 0;

    for(i = 0; i < 5; i++) {
        DArray_push(result, words[i]);
    }

    return result;
}

void random_gen_str(char *s, const int len) {
    const char alpha_and_num[] =
        "0123456789"
        "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
        "abcdefghijklmnopqrstuvwxyz";

    for (int i = 0; i < len; ++i) {
        s[i] = alpha_and_num[rand() % (sizeof(alpha_and_num) - 1)];
    }
    s[len] = '\0';
#ifdef DEBUG
    printf("[GENERATING STRING AT INDEX %d,len=%d]=>",++global_idx,len);
    puts(s);
#endif
}
DArray *create_words_by_gen(int size)
{
        DArray *words = DArray_create(0,size);
        char *auto_str=NULL;
        int i,len;
        for(i=0;i<size;i++){
                len = (rand() % STR_LEN)+1;
                if(len==1)len++;
                auto_str = malloc(sizeof(len));
                random_gen_str(auto_str,len-1);
                DArray_push(words,auto_str);
        }
        return words;
}

int is_sorted(DArray *array)
{
    int i = 0;

    for(i = 0; i < DArray_count(array) - 1; i++) {
        if(strcmp(DArray_get(array, i), DArray_get(array, i+1)) > 0) {
            return 0;
        }
    }

    return 1;
}

char *run_sort_test(int (*func)(DArray *, DArray_compare), const char *name)
{
    clock_t begin,end;
    FILE *file;
    double avg_processing_time;
    //DArray *words = create_words();
    DArray *words = NULL;
    int i,size=10,run_times=10;
    while(size<=NEW_NUM_VALUES){
	    i=0;avg_processing_time=0.0;
	    if(size==1010)size=1000;
	    while(i++<run_times){
		    words = create_words_by_gen(size);
		    mu_assert(!is_sorted(words), "Words should start not sorted.");
		    debug("--- Testing %s sorting algorithm", name);
		    begin=clock();
		    int rc = func(words, (DArray_compare)testcmp);
		    end=clock();
    	            mu_assert(rc == 0, "sort failed");
		    mu_assert(is_sorted(words),"Words aren't sorted after sort.");
		    avg_processing_time+=(double)(end-begin)/CLOCKS_PER_SEC;
		    DArray_destroy(words);
		    words=NULL;
	    }
	    if(strcmp(name,"qsort")==0){
		file = fopen("tests/qsort_ds","a");
	    } 
	    else if(strcmp(name,"heapsort")==0){
		    file = fopen("tests/heapsort_ds","a");
	    }
	    else if(strcmp(name,"mergesort")==0){
		    file = fopen("tests/mergesort_ds","a");
	    }
	    else if(strcmp(name,"custom_qsort")==0){
		    file = fopen("tests/myqsort_ds","a");
	    }
	    else if(strcmp(name,"custom_heapsort")==0){
		    file = fopen("tests/myheapsort_ds","a");
	    }
	    else if(strcmp(name,"custom_mergesort")==0){
		    file = fopen("tests/mymergesort_ds","a");
	    }
    	    fprintf(file,"%d %.10f\n",size,avg_processing_time/(double)run_times);
	    size+=50;
	    fclose(file);
    }

    return NULL;
}

char *test_qsort()
{
    return run_sort_test(DArray_qsort, "qsort");
}

char *test_heapsort()
{
    return run_sort_test(DArray_heapsort, "heapsort");
}

char *test_mergesort()
{
    return run_sort_test(DArray_mergesort, "mergesort");
}

char *test_custom_qsort()
{
	return run_sort_test(Custom_DArray_qsort, "custom_qsort");
}

char *test_custom_heapsort()
{
	return run_sort_test(Custom_DArray_heapsort, "custom_heapsort");
}

char *test_custom_mergesort()
{
	return run_sort_test(Custom_DArray_mergesort,"custom_mergesort");
}

char * all_tests()
{
    mu_suite_start();

    mu_run_test(test_qsort);
    mu_run_test(test_heapsort);
    mu_run_test(test_mergesort);
	
    mu_run_test(test_custom_qsort);
    mu_run_test(test_custom_mergesort);
    mu_run_test(test_custom_heapsort);
    return NULL;
}

RUN_TESTS(all_tests);
