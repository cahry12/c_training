(

echo 'set output "tests/perforamnce_sorting_with_different_size(totally_four_sorting).png"'
echo 'set terminal png'
echo 'set xlabel "The size of elements"'
echo 'set ylabel "Avg. processing time(sec)"'
echo 'set xrange [ 10:1000 ]'
echo 'set style data lines' 
echo 'set grid'
echo 'plot "tests/qsort_ds" using 1:2 title "Quick sort(library)" with linespoints, \'
echo ' "tests/myqsort_ds" using 1:2 title "Quick sort" with linespoints, \'
echo ' "tests/mergesort_ds" using 1:2 title "Merge sort(library)" with linespoints, \'
echo ' "tests/mymergesort_ds" using 1:2 title "Merge sort" with linespoints, \'
echo ' "tests/heapsort_ds" using 1:2 title "Heap sort(library)" with linespoints, \'
echo ' "tests/myheapsort_ds" using 1:2 title "Heap sort" with linespoints'
echo 'q'
) | gnuplot
