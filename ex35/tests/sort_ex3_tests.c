#include "minunit.h"
#include <lcthw/darray_algos.h>
#define NUM_OF_ELEMENTS 50
#define STRING_SIZE 10
int testcmp(char **a, char **b)
{
    return strcmp(*a, *b);
}

DArray *create_words()
{
    DArray *result = DArray_create(0, 5);
    char *words[] = {"asdfasfd", "werwar", "13234", "asdfasfd", "oioj"};
    int i = 0;

    for(i = 0; i < 5; i++) {
        DArray_push(result, words[i]);
    }

    return result;
}

int is_sorted(DArray *array)
{
    int i = 0;

    for(i = 0; i < DArray_count(array) - 1; i++) {
        if(strcmp(DArray_get(array, i), DArray_get(array, i+1)) > 0) {
            return 0;
        }
    }

    return 1;
}

char *run_sort_test(int (*func)(DArray *, DArray_compare), const char *name)
{
    DArray *words = create_words();
    mu_assert(!is_sorted(words), "Words should start not sorted.");

    debug("--- Testing %s sorting algorithm", name);
    int rc = func(words, (DArray_compare)testcmp);
    mu_assert(rc == 0, "sort failed");
    mu_assert(is_sorted(words), "didn't sort it");

    DArray_destroy(words);

    return NULL;
}

char *test_qsort()
{
    return run_sort_test(DArray_qsort, "qsort");
}

char *test_heapsort()
{
    return run_sort_test(DArray_heapsort, "heapsort");
}

char *test_mergesort()
{
    return run_sort_test(DArray_mergesort, "mergesort");
}
void random_gen_str(char *s, const int len) {
    const char alpha_and_num[] =
        "0123456789"
        "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
        "abcdefghijklmnopqrstuvwxyz";

    for (int i = 0; i < len; ++i) {
        s[i] = alpha_and_num[rand() % (sizeof(alpha_and_num) - 1)];
    }
    s[len] = '\0';
}
char *test_add_sort()
{
	DArray *words = create_words();
	mu_assert(!is_sorted(words), "Words should start not sorted.");
	debug("--- Testing quick sorting algorithm");
	void **elements;
	elements = malloc(NUM_OF_ELEMENTS*sizeof(void **));
	for(int i=0;i<NUM_OF_ELEMENTS;i++){
		elements[i]=malloc(STRING_SIZE*sizeof(void *));
		random_gen_str(elements[i],STRING_SIZE-1);	
	}
 	int rc = DArray_sort_add(words,elements,NUM_OF_ELEMENTS,(DArray_compare)testcmp);
	mu_assert(rc == 0, "sort failed");
 	mu_assert(is_sorted(words), "didn't sort it");
 	DArray_destroy(words);
	for(int i=0;i<NUM_OF_ELEMENTS;i++)
		free(elements[i]);
	free(elements);
	return NULL;
}

char * all_tests()
{
    mu_suite_start();

    mu_run_test(test_qsort);
    mu_run_test(test_heapsort);
    mu_run_test(test_mergesort);

    mu_run_test(test_add_sort);
    return NULL;
}

RUN_TESTS(all_tests);
